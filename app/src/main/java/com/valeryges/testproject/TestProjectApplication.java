package com.valeryges.testproject;

import android.app.Application;

import com.valeryges.testproject.injections.components.AppComponent;
import com.valeryges.testproject.injections.components.DaggerAppComponent;
import com.valeryges.testproject.injections.modules.AppModule;
import com.valeryges.testproject.injections.modules.DataBaseModule;
import com.valeryges.testproject.injections.modules.NetworkModule;


public class TestProjectApplication extends Application {

    private static TestProjectApplication instance;
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        appComponent =initDagger();
    }

    public static TestProjectApplication getInstance() {
        return instance;
    }

    protected AppComponent initDagger() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .dataBaseModule(new DataBaseModule())
                .build();
    }

    public static AppComponent getAppComponent() {
        return getInstance().appComponent;
    }
}
