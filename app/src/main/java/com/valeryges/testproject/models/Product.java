package com.valeryges.testproject.models;


import android.os.Parcel;

public class Product implements IProduct {

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    private Long serverId;
    private Long categoryId;
    private String title;
    private String description;
    private String price;
    private String currencyCode;
    private String url75x75;
    private String url170x135;
    private String url570xN;
    private String urlFullxfull;
    private boolean saved;

    public Product() {
    }

    public Product(Long serverId) {
        this.serverId = serverId;
    }

    public Long getServerId() {
        return serverId;
    }

    public void setServerId(Long serverId) {
        this.serverId = serverId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getUrl75x75() {
        return url75x75;
    }

    public void setUrl75x75(String url75x75) {
        this.url75x75 = url75x75;
    }

    public String getUrl170x135() {
        return url170x135;
    }

    public void setUrl170x135(String url170x135) {
        this.url170x135 = url170x135;
    }

    public String getUrl570xN() {
        return url570xN;
    }

    public void setUrl570xN(String url570xN) {
        this.url570xN = url570xN;
    }

    public String getUrlFullxfull() {
        return urlFullxfull;
    }

    public void setUrlFullxfull(String urlFullxfull) {
        this.urlFullxfull = urlFullxfull;
    }

    @Override
    public boolean isSaved() {
        return saved;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    protected Product(Parcel in) {
        serverId = in.readLong();
        categoryId = in.readLong();
        title = in.readString();
        description = in.readString();
        price = in.readString();
        currencyCode = in.readString();
        url75x75 = in.readString();
        url170x135 = in.readString();
        url570xN = in.readString();
        urlFullxfull = in.readString();
        saved = in.readInt() == 1;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(serverId);
        dest.writeLong(categoryId);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(price);
        dest.writeString(currencyCode);
        dest.writeString(url75x75);
        dest.writeString(url170x135);
        dest.writeString(url570xN);
        dest.writeString(urlFullxfull);
        dest.writeInt(saved ? 1 : 0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        return serverId.equals(product.serverId);

    }

    @Override
    public int hashCode() {
        return serverId.hashCode();
    }
}
