package com.valeryges.testproject.models;


import android.os.Parcelable;

/**
 * Encapsulates all information and methods for working with products
 */
public interface IProduct extends Parcelable {

    /**
     * Returns server id for product
     *
     * @return id value
     */
    Long getServerId();

    /**
     * Returns server id for category of product
     *
     * @return id value of category
     */
    Long getCategoryId();

    /**
     * Returns title of product
     *
     * @return Title
     */
    String getTitle();

    /**
     * Returns Description of product
     *
     * @return description
     */
    String getDescription();

    /**
     * Returns price of product
     *
     * @return price
     */
    String getPrice();

    /**
     * Returns currency code for price. For example "USD"
     *
     * @return Currency code
     */
    String getCurrencyCode();

    /**
     * returns {@link String} presentation of url to picture of product
     *
     * @return url to picture with 75 width and 75 height
     */
    String getUrl75x75();

    /**
     * Returns {@link String} presentation of url to picture of product
     *
     * @return url to picture with 170 width and 135 height
     */
    String getUrl170x135();

    /**
     * Returns {@link String} presentation of url to picture of product
     *
     * @return url to picture with 570 width
     */
    String getUrl570xN();

    /**
     * Returns {@link String} presentation of url to picture of product
     *
     * @return url to full size picture
     */
    String getUrlFullxfull();

    /**
     * Returns information about is product saved by user
     *
     * @return true if saved and false in another case
     */
    boolean isSaved();

    /**
     * Setter for saved status of product
     *
     * @param saved boolean presentation about state of product
     */
    void setSaved(boolean saved);

}
