package com.valeryges.testproject.models;


import android.os.Parcelable;

/**
 * Encapsulates all information and methods for working with categories
 */
public interface ICategory extends Parcelable {

    /**
     * Returns server id for category
     *
     * @return id value
     */
    Long getServerId();

    /**
     * Name of category for sending to server
     *
     * @return Name of category
     */
    String getName();

    /**
     * Name of category for displaying to user
     *
     * @return Long name of category
     */
    String getLongName();
}
