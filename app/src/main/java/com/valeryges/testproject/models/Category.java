package com.valeryges.testproject.models;


import android.os.Parcel;

public class Category implements ICategory {

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    private Long serverId;
    private String name;
    private String longName;

    public Category(Long serverId, String longName) {
        this(serverId);
        this.longName = longName;
    }

    public Category(Long serverId) {
        this.serverId = serverId;
    }

    public Category() {
    }

    public Long getServerId() {
        return serverId;
    }

    public void setServerId(Long serverId) {
        this.serverId = serverId;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    protected Category(Parcel in) {
        serverId = in.readLong();
        name = in.readString();
        longName = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(serverId);
        dest.writeString(name);
        dest.writeString(longName);
    }

    @Override
    public String toString() {
        return longName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;

        return serverId.equals(category.serverId);

    }

    @Override
    public int hashCode() {
        return serverId.hashCode();
    }
}
