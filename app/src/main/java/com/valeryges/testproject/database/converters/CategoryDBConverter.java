package com.valeryges.testproject.database.converters;


import android.support.annotation.Nullable;

import com.valeryges.testproject.database.dbmodels.CategoryDB;
import com.valeryges.testproject.models.Category;
import com.valeryges.testproject.models.ICategory;
import com.valeryges.testproject.models.converters.BaseConverter;

public class CategoryDBConverter extends BaseConverter<ICategory, CategoryDB>
        implements ICategoryDBConverter {

    @Override
    public CategoryDB convertINtoOUT(ICategory inObject) {
        return convertINtoOUT(inObject, null);
    }

    @Override
    public CategoryDB convertINtoOUT(ICategory inObject, @Nullable Object payload) {
        if (inObject == null) {
            return null;
        }
        CategoryDB categoryDB = payload instanceof CategoryDB
                ? (CategoryDB) payload
                : new CategoryDB();
        categoryDB.setServerId(inObject.getServerId());
        categoryDB.setName(inObject.getName());
        categoryDB.setLongName(inObject.getLongName());
        return categoryDB;
    }

    @Override
    public ICategory convertOUTtoIN(CategoryDB outObject) {
        return convertOUTtoIN(outObject, null);
    }

    @Override
    public ICategory convertOUTtoIN(CategoryDB inObject, @Nullable Object payload) {
        if (inObject == null) {
            return null;
        }
        Category category = payload instanceof Category
                ? (Category) payload
                : new Category();
        category.setServerId(inObject.getServerId());
        category.setName(inObject.getName());
        category.setLongName(inObject.getLongName());
        return category;
    }
}
