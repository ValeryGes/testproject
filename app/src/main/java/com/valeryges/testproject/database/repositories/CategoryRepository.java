package com.valeryges.testproject.database.repositories;

import com.valeryges.testproject.TestProjectApplication;
import com.valeryges.testproject.database.converters.ICategoryDBConverter;
import com.valeryges.testproject.database.dbmodels.CategoryDB;
import com.valeryges.testproject.database.dbmodels.CategoryDBDao;
import com.valeryges.testproject.models.ICategory;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;


public class CategoryRepository implements ICategoryRepository {

    @Inject
    CategoryDBDao categoryDBDao;
    @Inject
    ICategoryDBConverter categoryDBConverter;

    public CategoryRepository() {
        TestProjectApplication.getAppComponent().inject(this);
    }

    public Observable<ICategory> getByServerId(long serverId) {
        return getDBByServerIdObservable(serverId)
                .compose(categoryDBConverter.singleINtoOUT());
    }

    private Observable<CategoryDB> getDBByServerIdObservable(long serverId) {
        return Observable.fromCallable(() -> getDBByServerId(serverId));
    }

    private CategoryDB getDBByServerId(long serverId) {
        return categoryDBDao
                .queryBuilder()
                .where(CategoryDBDao.Properties.ServerId.eq(serverId))
                .build()
                .unique();
    }

    public Observable<ICategory> save(ICategory category) {
        return Observable
                .fromCallable(() -> category)
                .compose(categoryDBConverter.singleOUTtoIN())
                .compose(saveDB())
                .map(ignored -> category);
    }

    private Observable.Transformer<CategoryDB, Long> saveDB() {
        return categoryDBObservable -> categoryDBObservable.map(categoryDB -> {
            checkExistAndAddId(categoryDB);
            return categoryDBDao.insertOrReplace(categoryDB);
        });
    }

    public Observable<List<ICategory>> getAll() {
        return getAllDB()
                .compose(categoryDBConverter.listINtoOUT());
    }

    private Observable<List<CategoryDB>> getAllDB() {
        return Observable.fromCallable(categoryDBDao::loadAll);
    }

    public Observable<List<ICategory>> save(List<ICategory> categories) {
        return Observable.fromCallable(() -> categories)
                .compose(categoryDBConverter.listOUTtoIN())
                .map(categoryDBs -> {
                    for (CategoryDB categoryDB : categoryDBs) {
                        checkExistAndAddId(categoryDB);
                    }
                    return categoryDBs;
                }).flatMap(categoryDBs -> {
                    categoryDBDao.saveInTx(categoryDBs);
                    return Observable.just(categories);
                });
    }

    public Observable<Void> remove(ICategory category) {
        return Observable.fromCallable(() -> category)
                .compose(categoryDBConverter.singleOUTtoIN())
                .flatMap(categoryDB -> {
                    checkExistAndAddId(categoryDB);
                    categoryDBDao.delete(categoryDB);
                    return Observable.just(null);
                });
    }

    public Observable<Void> remove(List<ICategory> categories) {
        return Observable.fromCallable(() -> categories)
                .compose(categoryDBConverter.listOUTtoIN())
                .flatMap(categoryDBs -> {
                    for (CategoryDB categoryDB : categoryDBs) {
                        checkExistAndAddId(categoryDB);
                    }
                    categoryDBDao.deleteInTx(categoryDBs);
                    return Observable.just(null);
                });
    }

    private void checkExistAndAddId(CategoryDB categoryDB) {
        CategoryDB checkExist = getDBByServerId(categoryDB.getServerId());
        if (checkExist != null) {
            categoryDB.setId(checkExist.getId());
        }
    }
}
