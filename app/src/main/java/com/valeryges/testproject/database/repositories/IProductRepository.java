package com.valeryges.testproject.database.repositories;


import com.valeryges.testproject.models.IProduct;

import java.util.List;

import rx.Observable;

/**
 * Repository for {@link IProduct}. Extends of {@link IRepository}
 */
public interface IProductRepository extends IRepository<IProduct> {
    /**
     * Returns all stored products with {@link IProduct#isSaved()} value equals true
     *
     * @return Instance of {@link Observable} with {@link List} of {@link IProduct} entities.
     */
    Observable<List<IProduct>> getSavedProducts();
}
