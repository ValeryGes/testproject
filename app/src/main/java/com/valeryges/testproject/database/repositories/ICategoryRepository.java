package com.valeryges.testproject.database.repositories;

import com.valeryges.testproject.models.ICategory;

/**
 * Repository for {@link ICategory}. Extends of {@link IRepository}
 */
public interface ICategoryRepository extends IRepository<ICategory> {

}
