package com.valeryges.testproject.database.converters;


import android.support.annotation.Nullable;

import com.valeryges.testproject.database.dbmodels.ProductDB;
import com.valeryges.testproject.models.IProduct;
import com.valeryges.testproject.models.Product;
import com.valeryges.testproject.models.converters.BaseConverter;

public class ProductDBConverter extends BaseConverter<IProduct, ProductDB> implements IProductDBConverter {

    @Override
    public ProductDB convertINtoOUT(IProduct inObject) {
        return convertINtoOUT(inObject, null);
    }

    @Override
    public ProductDB convertINtoOUT(IProduct inObject, @Nullable Object payload) {
        if (inObject == null) {
            return null;
        }
        ProductDB productDB = payload instanceof ProductDB ? (ProductDB) payload : new ProductDB();
        productDB.setServerId(inObject.getServerId());
        productDB.setCategoryId(inObject.getCategoryId());
        productDB.setTitle(inObject.getTitle());
        productDB.setDescription(inObject.getDescription());
        productDB.setPrice(inObject.getPrice());
        productDB.setCurrencyCode(inObject.getCurrencyCode());
        productDB.setUrl75x75(inObject.getUrl75x75());
        productDB.setUrl170x135(inObject.getUrl170x135());
        productDB.setUrl570xN(inObject.getUrl570xN());
        productDB.setUrlFullxfull(inObject.getUrlFullxfull());
        productDB.setSaved(inObject.isSaved());
        return productDB;
    }

    @Override
    public IProduct convertOUTtoIN(ProductDB outObject) {
        return convertOUTtoIN(outObject, null);
    }

    @Override
    public IProduct convertOUTtoIN(ProductDB outObject, @Nullable Object payload) {
        if (outObject == null) {
            return null;
        }
        Product product = payload instanceof Product ? (Product) payload : new Product();
        product.setServerId(outObject.getServerId());
        product.setCategoryId(outObject.getCategoryId());
        product.setTitle(outObject.getTitle());
        product.setDescription(outObject.getDescription());
        product.setPrice(outObject.getPrice());
        product.setCurrencyCode(outObject.getCurrencyCode());
        product.setUrl75x75(outObject.getUrl75x75());
        product.setUrl170x135(outObject.getUrl170x135());
        product.setUrl570xN(outObject.getUrl570xN());
        product.setUrlFullxfull(outObject.getUrlFullxfull());
        product.setSaved(outObject.getSaved());
        return product;
    }
}
