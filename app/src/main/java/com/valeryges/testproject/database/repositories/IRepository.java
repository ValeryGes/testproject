package com.valeryges.testproject.database.repositories;


import java.util.List;

import rx.Observable;

/**
 * Encapsulate logic for working with Database with some entity type
 *
 * @param <T> Type of entity to work with database
 */
public interface IRepository<T> {
    /**
     * Returns entity by server id
     *
     * @param serverId Server id of searching entity. If there already is entity inside database.
     *                 It will be rewritten
     * @return instance of {@link Observable} with entity or null inside
     * if there isn't any with current server id
     */
    Observable<T> getByServerId(long serverId);

    /**
     * Save entity in database
     *
     * @param entity Entity to save
     * @return Instance of {@link Observable} with entity inside
     */
    Observable<T> save(T entity);

    /**
     * Returns Observable with all entities stored in Database
     *
     * @return Instance of {@link Observable} with {@link List} of entities. Can be empty.
     */
    Observable<List<T>> getAll();

    /**
     * Save list of entities in Database
     *
     * @param entities {@link List} with entities for save
     * @return Instance of {@link Observable} with {@link List} of entities.
     */
    Observable<List<T>> save(List<T> entities);

    /**
     * Remove entity from database
     *
     * @param entity - Entity for remove
     * @return Instance of {@link Observable} with value, you should ignore
     */
    Observable<Void> remove(T entity);

    /**
     * Remove entities from database
     *
     * @param entities - {@link List} with entities for remove
     * @return Instance of {@link Observable} with value, you should ignore
     */
    Observable<Void> remove(List<T> entities);
}
