package com.valeryges.testproject.database.dbmodels;


import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Unique;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Inner entity for storing products in database
 */
@Entity
public class ProductDB {
    @Id(autoincrement = true)
    Long id;

    @Unique
    Long ServerId;

    Long categoryId;

    String title;

    String description;

    String price;

    String currencyCode;

    String url75x75;

    String url170x135;

    String url570xN;

    String urlFullxfull;

    Boolean saved = false;

    @Generated(hash = 13671200)
    public ProductDB(Long id, Long ServerId, Long categoryId, String title,
                     String description, String price, String currencyCode, String url75x75,
                     String url170x135, String url570xN, String urlFullxfull,
                     Boolean saved) {
        this.id = id;
        this.ServerId = ServerId;
        this.categoryId = categoryId;
        this.title = title;
        this.description = description;
        this.price = price;
        this.currencyCode = currencyCode;
        this.url75x75 = url75x75;
        this.url170x135 = url170x135;
        this.url570xN = url570xN;
        this.urlFullxfull = urlFullxfull;
        this.saved = saved;
    }

    @Generated(hash = 1040855488)
    public ProductDB() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getServerId() {
        return this.ServerId;
    }

    public void setServerId(Long ServerId) {
        this.ServerId = ServerId;
    }

    public Long getCategoryId() {
        return this.categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCurrencyCode() {
        return this.currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getUrl75x75() {
        return this.url75x75;
    }

    public void setUrl75x75(String url75x75) {
        this.url75x75 = url75x75;
    }

    public String getUrl170x135() {
        return this.url170x135;
    }

    public void setUrl170x135(String url170x135) {
        this.url170x135 = url170x135;
    }

    public String getUrl570xN() {
        return this.url570xN;
    }

    public void setUrl570xN(String url570xN) {
        this.url570xN = url570xN;
    }

    public String getUrlFullxfull() {
        return this.urlFullxfull;
    }

    public void setUrlFullxfull(String urlFullxfull) {
        this.urlFullxfull = urlFullxfull;
    }

    public Boolean getSaved() {
        return this.saved;
    }

    public void setSaved(Boolean saved) {
        this.saved = saved;
    }
}
