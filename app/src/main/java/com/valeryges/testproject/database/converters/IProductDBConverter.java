package com.valeryges.testproject.database.converters;


import com.valeryges.testproject.database.dbmodels.ProductDB;
import com.valeryges.testproject.models.IProduct;
import com.valeryges.testproject.models.converters.IConverter;

/**
 * Converter for Database entity {@link IProduct}. Extends {@link IConverter}
 */
public interface IProductDBConverter extends IConverter<IProduct, ProductDB> {
}
