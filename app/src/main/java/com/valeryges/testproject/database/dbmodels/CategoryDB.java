package com.valeryges.testproject.database.dbmodels;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Unique;
import org.greenrobot.greendao.annotation.Generated;
/**
 * Inner entity for storing categories in database
 */
@Entity
public class CategoryDB {

    @Id(autoincrement = true)
    Long id;

    @Unique
    Long ServerId;

    String name;

    String longName;

    @Generated(hash = 1986212688)
    public CategoryDB(Long id, Long ServerId, String name, String longName) {
        this.id = id;
        this.ServerId = ServerId;
        this.name = name;
        this.longName = longName;
    }

    @Generated(hash = 2085389353)
    public CategoryDB() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getServerId() {
        return this.ServerId;
    }

    public void setServerId(Long ServerId) {
        this.ServerId = ServerId;
    }

    public String getLongName() {
        return this.longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
