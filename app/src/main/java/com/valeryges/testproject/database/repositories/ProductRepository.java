package com.valeryges.testproject.database.repositories;


import com.valeryges.testproject.TestProjectApplication;
import com.valeryges.testproject.database.converters.IProductDBConverter;
import com.valeryges.testproject.database.dbmodels.ProductDB;
import com.valeryges.testproject.database.dbmodels.ProductDBDao;
import com.valeryges.testproject.models.IProduct;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

public class ProductRepository implements IProductRepository {

    @Inject
    IProductDBConverter productDBConverter;
    @Inject
    ProductDBDao productDBDao;

    public ProductRepository() {
        TestProjectApplication.getAppComponent().inject(this);
    }

    public Observable<IProduct> getByServerId(long serverId) {
        return getDBByServerIdObservable(serverId)
                .compose(productDBConverter.singleINtoOUT());
    }

    private Observable<ProductDB> getDBByServerIdObservable(long serverId) {
        return Observable.fromCallable(() -> getDBByServerId(serverId));
    }

    private ProductDB getDBByServerId(long serverId) {
        return productDBDao
                .queryBuilder()
                .where(ProductDBDao.Properties.ServerId.eq(serverId))
                .build()
                .unique();
    }

    public Observable<IProduct> save(IProduct product) {
        return Observable
                .fromCallable(() -> product)
                .compose(productDBConverter.singleOUTtoIN())
                .compose(saveDB())
                .map(ignored -> product);
    }

    private Observable.Transformer<ProductDB, Long> saveDB() {
        return categoryDBObservable -> categoryDBObservable.map(categoryDB -> {
            checkExistAndAddId(categoryDB);
            return productDBDao.insertOrReplace(categoryDB);
        });
    }

    public Observable<List<IProduct>> getAll() {
        return getAllDB()
                .compose(productDBConverter.listINtoOUT());
    }

    @Override
    public Observable<List<IProduct>> getSavedProducts() {
        return Observable.just(productDBDao
                .queryBuilder()
                .where(ProductDBDao.Properties.Saved.eq(true))
                .build()
                .list())
                .compose(productDBConverter.listINtoOUT());
    }

    private Observable<List<ProductDB>> getAllDB() {
        return Observable.fromCallable(productDBDao::loadAll);
    }

    public Observable<List<IProduct>> save(List<IProduct> products) {
        return Observable.fromCallable(() -> products)
                .compose(productDBConverter.listOUTtoIN())
                .map(productDBs -> {
                    for (ProductDB productDB : productDBs) {
                        checkExistAndAddId(productDB);
                    }
                    return productDBs;
                }).flatMap(productsDB1 -> {
                    productDBDao.saveInTx(productsDB1);
                    return Observable.just(products);
                });
    }

    public Observable<Void> remove(IProduct product) {
        return Observable.fromCallable(() -> product)
                .compose(productDBConverter.singleOUTtoIN())
                .flatMap(productDB -> {
                    checkExistAndAddId(productDB);
                    productDBDao.delete(productDB);
                    return Observable.just(null);
                });
    }

    public Observable<Void> remove(List<IProduct> products) {
        return Observable.fromCallable(() -> products)
                .compose(productDBConverter.listOUTtoIN())
                .flatMap(productDBs -> {
                    for (ProductDB productDB : productDBs) {
                        checkExistAndAddId(productDB);
                    }
                    productDBDao.deleteInTx(productDBs);
                    return Observable.just(null);
                });
    }

    private void checkExistAndAddId(ProductDB productDB) {
        ProductDB checkExist = getDBByServerId(productDB.getServerId());
        if (checkExist != null) {
            productDB.setId(checkExist.getId());
        }
    }
}
