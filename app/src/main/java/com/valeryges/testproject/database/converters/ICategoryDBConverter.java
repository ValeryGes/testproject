package com.valeryges.testproject.database.converters;


import com.valeryges.testproject.database.dbmodels.CategoryDB;
import com.valeryges.testproject.models.ICategory;
import com.valeryges.testproject.models.converters.IConverter;

/**
 * Converter for Database entity {@link ICategory}. Extends {@link IConverter}
 */
public interface ICategoryDBConverter extends IConverter<ICategory, CategoryDB> {
}
