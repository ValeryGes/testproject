package com.valeryges.testproject.funcs;

/**
 * Simple functional interface without arguments and return types
 */
@FunctionalInterface
public interface Func {
    void call();
}
