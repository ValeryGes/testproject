package com.valeryges.testproject.network.responces;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.valeryges.testproject.network.api.IProductsApi;
import com.valeryges.testproject.network.beans.ProductBean;

import java.io.Serializable;
import java.util.List;

/**
 * Generated entity of response
 * for {@link IProductsApi#getProducts(String, String, String, int, int, String)}
 */
public class ProductResponse implements Serializable {

    @Expose
    @SerializedName("count")
    private Integer count;
    @Expose
    @SerializedName("params")
    private ParamsBean params;
    @Expose
    @SerializedName("type")
    private String type;
    @Expose
    @SerializedName("pagination")
    private PaginationBean pagination;
    @Expose
    @SerializedName("results")
    private List<ProductBean> results;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public ParamsBean getParams() {
        return params;
    }

    public void setParams(ParamsBean params) {
        this.params = params;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public PaginationBean getPagination() {
        return pagination;
    }

    public void setPagination(PaginationBean pagination) {
        this.pagination = pagination;
    }

    public List<ProductBean> getResults() {
        return results;
    }

    public void setResults(List<ProductBean> results) {
        this.results = results;
    }

    public static class ParamsBean {

        @Expose
        @SerializedName("limit")
        private Integer limit;
        @Expose
        @SerializedName("offset")
        private Integer offset;
        @Expose
        @SerializedName("page")
        private Object page;
        @Expose
        @SerializedName("keywords")
        private String keywords;
        @Expose
        @SerializedName("sort_on")
        private String sortOn;
        @Expose
        @SerializedName("sort_order")
        private String sortOrder;
        @Expose
        @SerializedName("min_price")
        private Object minPrice;
        @Expose
        @SerializedName("max_price")
        private Object maxPrice;
        @Expose
        @SerializedName("color")
        private Object color;
        @Expose
        @SerializedName("color_accuracy")
        private Integer colorAccuracy;
        @Expose
        @SerializedName("tags")
        private Object tags;
        @Expose
        @SerializedName("category")
        private String category;
        @Expose
        @SerializedName("location")
        private Object location;
        @Expose
        @SerializedName("lat")
        private Object lat;
        @Expose
        @SerializedName("lon")
        private Object lon;
        @Expose
        @SerializedName("region")
        private Object region;
        @Expose
        @SerializedName("geo_level")
        private String geoLevel;
        @Expose
        @SerializedName("accepts_gift_cards")
        private String acceptsGiftCards;
        @Expose
        @SerializedName("translate_keywords")
        private String translateKeywords;

        public Integer getLimit() {
            return limit;
        }

        public void setLimit(Integer limit) {
            this.limit = limit;
        }

        public Integer getOffset() {
            return offset;
        }

        public void setOffset(Integer offset) {
            this.offset = offset;
        }

        public Object getPage() {
            return page;
        }

        public void setPage(Object page) {
            this.page = page;
        }

        public String getKeywords() {
            return keywords;
        }

        public void setKeywords(String keywords) {
            this.keywords = keywords;
        }

        public String getSortOn() {
            return sortOn;
        }

        public void setSortOn(String sortOn) {
            this.sortOn = sortOn;
        }

        public String getSortOrder() {
            return sortOrder;
        }

        public void setSortOrder(String sortOrder) {
            this.sortOrder = sortOrder;
        }

        public Object getMinPrice() {
            return minPrice;
        }

        public void setMinPrice(Object minPrice) {
            this.minPrice = minPrice;
        }

        public Object getMaxPrice() {
            return maxPrice;
        }

        public void setMaxPrice(Object maxPrice) {
            this.maxPrice = maxPrice;
        }

        public Object getColor() {
            return color;
        }

        public void setColor(Object color) {
            this.color = color;
        }

        public Integer getColorAccuracy() {
            return colorAccuracy;
        }

        public void setColorAccuracy(Integer colorAccuracy) {
            this.colorAccuracy = colorAccuracy;
        }

        public Object getTags() {
            return tags;
        }

        public void setTags(Object tags) {
            this.tags = tags;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public Object getLocation() {
            return location;
        }

        public void setLocation(Object location) {
            this.location = location;
        }

        public Object getLat() {
            return lat;
        }

        public void setLat(Object lat) {
            this.lat = lat;
        }

        public Object getLon() {
            return lon;
        }

        public void setLon(Object lon) {
            this.lon = lon;
        }

        public Object getRegion() {
            return region;
        }

        public void setRegion(Object region) {
            this.region = region;
        }

        public String getGeoLevel() {
            return geoLevel;
        }

        public void setGeoLevel(String geoLevel) {
            this.geoLevel = geoLevel;
        }

        public String getAcceptsGiftCards() {
            return acceptsGiftCards;
        }

        public void setAcceptsGiftCards(String acceptsGiftCards) {
            this.acceptsGiftCards = acceptsGiftCards;
        }

        public String getTranslateKeywords() {
            return translateKeywords;
        }

        public void setTranslateKeywords(String translateKeywords) {
            this.translateKeywords = translateKeywords;
        }
    }

    public static class PaginationBean {

        @Expose
        @SerializedName("effective_limit")
        private Integer effectiveLimit;
        @Expose
        @SerializedName("effective_offset")
        private Integer effectiveOffset;
        @Expose
        @SerializedName("next_offset")
        private Integer nextOffset;
        @Expose
        @SerializedName("effective_page")
        private Integer effectivePage;
        @Expose
        @SerializedName("next_page")
        private Integer nextPage;

        public Integer getEffectiveLimit() {
            return effectiveLimit;
        }

        public void setEffectiveLimit(Integer effectiveLimit) {
            this.effectiveLimit = effectiveLimit;
        }

        public Integer getEffectiveOffset() {
            return effectiveOffset;
        }

        public void setEffectiveOffset(Integer effectiveOffset) {
            this.effectiveOffset = effectiveOffset;
        }

        public Integer getNextOffset() {
            return nextOffset;
        }

        public void setNextOffset(Integer nextOffset) {
            this.nextOffset = nextOffset;
        }

        public Integer getEffectivePage() {
            return effectivePage;
        }

        public void setEffectivePage(Integer effectivePage) {
            this.effectivePage = effectivePage;
        }

        public Integer getNextPage() {
            return nextPage;
        }

        public void setNextPage(Integer nextPage) {
            this.nextPage = nextPage;
        }
    }

}
