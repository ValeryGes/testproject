package com.valeryges.testproject.network.modules;


import com.valeryges.testproject.BuildConfig;
import com.valeryges.testproject.TestProjectApplication;
import com.valeryges.testproject.models.ICategory;
import com.valeryges.testproject.network.api.ICategoriesApi;
import com.valeryges.testproject.network.beans.converters.ICategoryBeanConverter;
import com.valeryges.testproject.network.responces.CategoryResponse;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

public class CategoriesModule extends BaseRxModule<ICategoriesApi> implements ICategoriesModule {

    @Inject
    ICategoryBeanConverter categoriesBeanConverter;

    public CategoriesModule(ICategoriesApi module) {
        super(module);
        TestProjectApplication.getAppComponent().inject(this);
    }


    public Observable<List<ICategory>> getCategories() {
        return module
                .findAllTopCategory(BuildConfig.ACCESS_TOKEN)
                .map(CategoryResponse::getResults)
                .compose(categoriesBeanConverter.listOUTtoIN());
    }
}
