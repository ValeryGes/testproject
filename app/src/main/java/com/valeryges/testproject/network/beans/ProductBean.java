package com.valeryges.testproject.network.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Generated Network bean for Product
 */
public class ProductBean {


    @Expose
    @SerializedName("listing_id")
    private Long listingId;
    @Expose
    @SerializedName("state")
    private String state;
    @Expose
    @SerializedName("user_id")
    private Long userId;
    @Expose
    @SerializedName("category_id")
    private Long categoryId;
    @Expose
    @SerializedName("title")
    private String title;
    @Expose
    @SerializedName("description")
    private String description;
    @Expose
    @SerializedName("creation_tsz")
    private Integer creationTsz;
    @Expose
    @SerializedName("ending_tsz")
    private Integer endingTsz;
    @Expose
    @SerializedName("original_creation_tsz")
    private Integer originalCreationTsz;
    @Expose
    @SerializedName("last_modified_tsz")
    private Integer lastModifiedTsz;
    @Expose
    @SerializedName("price")
    private String price;
    @Expose
    @SerializedName("currency_code")
    private String currencyCode;
    @Expose
    @SerializedName("quantity")
    private Integer quantity;
    @Expose
    @SerializedName("shop_section_id")
    private Object shopSectionId;
    @Expose
    @SerializedName("featured_rank")
    private Object featuredRank;
    @Expose
    @SerializedName("state_tsz")
    private Integer stateTsz;
    @Expose
    @SerializedName("url")
    private String url;
    @SerializedName("views")
    @Expose
    private Integer views;
    @SerializedName("num_favorers")
    @Expose
    private Integer numFavorers;
    @Expose
    @SerializedName("shipping_template_id")
    private Long shippingTemplateId;
    @Expose
    @SerializedName("processing_min")
    private Integer processingMin;
    @Expose
    @SerializedName("processing_max")
    private Integer processingMax;
    @Expose
    @SerializedName("who_made")
    private String whoMade;
    @Expose
    @SerializedName("is_supply")
    private String isSupply;
    @Expose
    @SerializedName("when_made")
    private String whenMade;
    @Expose
    @SerializedName("item_weight")
    private String itemWeight;
    @Expose
    @SerializedName("item_weight_units")
    private Object itemWeightUnits;
    @Expose
    @SerializedName("item_length")
    private String itemLength;
    @Expose
    @SerializedName("item_width")
    private String itemWidth;
    @Expose
    @SerializedName("item_height")
    private String itemHeight;
    @Expose
    @SerializedName("item_dimensions_unit")
    private String itemDimensionsUnit;
    @Expose
    @SerializedName("is_private")
    private Boolean isPrivate;
    @Expose
    @SerializedName("recipient")
    private Object recipient;
    @Expose
    @SerializedName("occasion")
    private Object occasion;
    @Expose
    @SerializedName("style")
    private Object style;
    @Expose
    @SerializedName("non_taxable")
    private Boolean nonTaxable;
    @Expose
    @SerializedName("is_customizable")
    private Boolean isCustomizable;
    @Expose
    @SerializedName("is_digital")
    private Boolean isDigital;
    @Expose
    @SerializedName("file_data")
    private String fileData;
    @Expose
    @SerializedName("language")
    private String language;
    @Expose
    @SerializedName("has_variations")
    private Boolean hasVariations;
    @Expose
    @SerializedName("taxonomy_id")
    private Integer taxonomyId;
    @Expose
    @SerializedName("used_manufacturer")
    private Boolean usedManufacturer;
    @Expose
    @SerializedName("suggested_taxonomy_id")
    private Integer suggestedTaxonomyId;
    @Expose
    @SerializedName("tags")
    private List<?> tags;
    @Expose
    @SerializedName("category_path")
    private List<String> categoryPath;
    @Expose
    @SerializedName("category_path_ids")
    private List<Integer> categoryPathIds;
    @Expose
    @SerializedName("materials")
    private List<?> materials;
    @Expose
    @SerializedName("taxonomy_path")
    private List<String> taxonomyPath;
    @Expose
    @SerializedName("MainImage")
    private MainImageBean MainImage;

    public Long getListingId() {
        return listingId;
    }

    public void setListingId(Long listingId) {
        this.listingId = listingId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCreationTsz() {
        return creationTsz;
    }

    public void setCreationTsz(Integer creationTsz) {
        this.creationTsz = creationTsz;
    }

    public Integer getEndingTsz() {
        return endingTsz;
    }

    public void setEndingTsz(Integer endingTsz) {
        this.endingTsz = endingTsz;
    }

    public Integer getOriginalCreationTsz() {
        return originalCreationTsz;
    }

    public void setOriginalCreationTsz(Integer originalCreationTsz) {
        this.originalCreationTsz = originalCreationTsz;
    }

    public Integer getLastModifiedTsz() {
        return lastModifiedTsz;
    }

    public void setLastModifiedTsz(Integer lastModifiedTsz) {
        this.lastModifiedTsz = lastModifiedTsz;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Object getShopSectionId() {
        return shopSectionId;
    }

    public void setShopSectionId(Object shopSectionId) {
        this.shopSectionId = shopSectionId;
    }

    public Object getFeaturedRank() {
        return featuredRank;
    }

    public void setFeaturedRank(Object featuredRank) {
        this.featuredRank = featuredRank;
    }

    public Integer getStateTsz() {
        return stateTsz;
    }

    public void setStateTsz(Integer stateTsz) {
        this.stateTsz = stateTsz;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }

    public Integer getNumFavorers() {
        return numFavorers;
    }

    public void setNumFavorers(Integer numFavorers) {
        this.numFavorers = numFavorers;
    }

    public Long getShippingTemplateId() {
        return shippingTemplateId;
    }

    public void setShippingTemplateId(Long shippingTemplateId) {
        this.shippingTemplateId = shippingTemplateId;
    }

    public Integer getProcessingMin() {
        return processingMin;
    }

    public void setProcessingMin(Integer processingMin) {
        this.processingMin = processingMin;
    }

    public Integer getProcessingMax() {
        return processingMax;
    }

    public void setProcessingMax(Integer processingMax) {
        this.processingMax = processingMax;
    }

    public String getWhoMade() {
        return whoMade;
    }

    public void setWhoMade(String whoMade) {
        this.whoMade = whoMade;
    }

    public String getIsSupply() {
        return isSupply;
    }

    public void setIsSupply(String isSupply) {
        this.isSupply = isSupply;
    }

    public String getWhenMade() {
        return whenMade;
    }

    public void setWhenMade(String whenMade) {
        this.whenMade = whenMade;
    }

    public String getItemWeight() {
        return itemWeight;
    }

    public void setItemWeight(String itemWeight) {
        this.itemWeight = itemWeight;
    }

    public Object getItemWeightUnits() {
        return itemWeightUnits;
    }

    public void setItemWeightUnits(Object itemWeightUnits) {
        this.itemWeightUnits = itemWeightUnits;
    }

    public String getItemLength() {
        return itemLength;
    }

    public void setItemLength(String itemLength) {
        this.itemLength = itemLength;
    }

    public String getItemWidth() {
        return itemWidth;
    }

    public void setItemWidth(String itemWidth) {
        this.itemWidth = itemWidth;
    }

    public String getItemHeight() {
        return itemHeight;
    }

    public void setItemHeight(String itemHeight) {
        this.itemHeight = itemHeight;
    }

    public String getItemDimensionsUnit() {
        return itemDimensionsUnit;
    }

    public void setItemDimensionsUnit(String itemDimensionsUnit) {
        this.itemDimensionsUnit = itemDimensionsUnit;
    }

    public Boolean isIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(Boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public Object getRecipient() {
        return recipient;
    }

    public void setRecipient(Object recipient) {
        this.recipient = recipient;
    }

    public Object getOccasion() {
        return occasion;
    }

    public void setOccasion(Object occasion) {
        this.occasion = occasion;
    }

    public Object getStyle() {
        return style;
    }

    public void setStyle(Object style) {
        this.style = style;
    }

    public Boolean isNonTaxable() {
        return nonTaxable;
    }

    public void setNonTaxable(Boolean nonTaxable) {
        this.nonTaxable = nonTaxable;
    }

    public Boolean isIsCustomizable() {
        return isCustomizable;
    }

    public void setIsCustomizable(Boolean isCustomizable) {
        this.isCustomizable = isCustomizable;
    }

    public Boolean isIsDigital() {
        return isDigital;
    }

    public void setIsDigital(Boolean isDigital) {
        this.isDigital = isDigital;
    }

    public String getFileData() {
        return fileData;
    }

    public void setFileData(String fileData) {
        this.fileData = fileData;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Boolean isHasVariations() {
        return hasVariations;
    }

    public void setHasVariations(Boolean hasVariations) {
        this.hasVariations = hasVariations;
    }

    public Integer getTaxonomyId() {
        return taxonomyId;
    }

    public void setTaxonomyId(Integer taxonomyId) {
        this.taxonomyId = taxonomyId;
    }

    public Boolean isUsedManufacturer() {
        return usedManufacturer;
    }

    public void setUsedManufacturer(Boolean usedManufacturer) {
        this.usedManufacturer = usedManufacturer;
    }

    public Integer getSuggestedTaxonomyId() {
        return suggestedTaxonomyId;
    }

    public void setSuggestedTaxonomyId(Integer suggestedTaxonomyId) {
        this.suggestedTaxonomyId = suggestedTaxonomyId;
    }

    public List<?> getTags() {
        return tags;
    }

    public void setTags(List<?> tags) {
        this.tags = tags;
    }

    public List<String> getCategoryPath() {
        return categoryPath;
    }

    public void setCategoryPath(List<String> categoryPath) {
        this.categoryPath = categoryPath;
    }

    public List<Integer> getCategoryPathIds() {
        return categoryPathIds;
    }

    public void setCategoryPathIds(List<Integer> categoryPathIds) {
        this.categoryPathIds = categoryPathIds;
    }

    public List<?> getMaterials() {
        return materials;
    }

    public void setMaterials(List<?> materials) {
        this.materials = materials;
    }

    public List<String> getTaxonomyPath() {
        return taxonomyPath;
    }

    public void setTaxonomyPath(List<String> taxonomyPath) {
        this.taxonomyPath = taxonomyPath;
    }

    public MainImageBean getMainImage() {
        return MainImage;
    }

    public void setMainImage(MainImageBean mainImage) {
        MainImage = mainImage;
    }

    public static class MainImageBean {

        @Expose
        @SerializedName("listing_image_id")
        private Long listingImageId;
        @Expose
        @SerializedName("hex_code")
        private String hexCode;
        @Expose
        @SerializedName("red")
        private Integer red;
        @Expose
        @SerializedName("green")
        private Integer green;
        @Expose
        @SerializedName("blue")
        private Integer blue;
        @Expose
        @SerializedName("hue")
        private Integer hue;
        @Expose
        @SerializedName("saturation")
        private Integer saturation;
        @Expose
        @SerializedName("brightness")
        private Integer brightness;
        @Expose
        @SerializedName("is_black_and_white")
        private Boolean isBlackAndWhite;
        @Expose
        @SerializedName("creation_tsz")
        private Double creationTsz;
        @Expose
        @SerializedName("listing_id")
        private Long listingId;
        @Expose
        @SerializedName("rank")
        private Integer rank;
        @Expose
        @SerializedName("url_75x75")
        private String url75x75;
        @Expose
        @SerializedName("url_170x135")
        private String url170x135;
        @Expose
        @SerializedName("url_570xN")
        private String url570xN;
        @Expose
        @SerializedName("url_fullxfull")
        private String urlFullxfull;
        @Expose
        @SerializedName("full_height")
        private Integer fullHeight;
        @Expose
        @SerializedName("full_width")
        private Integer fullWidth;

        public Long getListingImageId() {
            return listingImageId;
        }

        public void setListingImageId(Long listingImageId) {
            this.listingImageId = listingImageId;
        }

        public String getHexCode() {
            return hexCode;
        }

        public void setHexCode(String hexCode) {
            this.hexCode = hexCode;
        }

        public Integer getRed() {
            return red;
        }

        public void setRed(Integer red) {
            this.red = red;
        }

        public Integer getGreen() {
            return green;
        }

        public void setGreen(Integer green) {
            this.green = green;
        }

        public Integer getBlue() {
            return blue;
        }

        public void setBlue(Integer blue) {
            this.blue = blue;
        }

        public Integer getHue() {
            return hue;
        }

        public void setHue(Integer hue) {
            this.hue = hue;
        }

        public Integer getSaturation() {
            return saturation;
        }

        public void setSaturation(Integer saturation) {
            this.saturation = saturation;
        }

        public Integer getBrightness() {
            return brightness;
        }

        public void setBrightness(Integer brightness) {
            this.brightness = brightness;
        }

        public Boolean getIsBlackAndWhite() {
            return isBlackAndWhite;
        }

        public void setIsBlackAndWhite(Boolean isBlackAndWhite) {
            this.isBlackAndWhite = isBlackAndWhite;
        }

        public Double getCreationTsz() {
            return creationTsz;
        }

        public void setCreationTsz(Double creationTsz) {
            this.creationTsz = creationTsz;
        }

        public Long getListingId() {
            return listingId;
        }

        public void setListingId(Long listingId) {
            this.listingId = listingId;
        }

        public Integer getRank() {
            return rank;
        }

        public void setRank(Integer rank) {
            this.rank = rank;
        }

        public String getUrl75x75() {
            return url75x75;
        }

        public void setUrl75x75(String url75x75) {
            this.url75x75 = url75x75;
        }

        public String getUrl170x135() {
            return url170x135;
        }

        public void setUrl170x135(String url170x135) {
            this.url170x135 = url170x135;
        }

        public String getUrl570xN() {
            return url570xN;
        }

        public void setUrl570xN(String url570xN) {
            this.url570xN = url570xN;
        }

        public String getUrlFullxfull() {
            return urlFullxfull;
        }

        public void setUrlFullxfull(String urlFullxfull) {
            this.urlFullxfull = urlFullxfull;
        }

        public Integer getFullHeight() {
            return fullHeight;
        }

        public void setFullHeight(Integer fullHeight) {
            this.fullHeight = fullHeight;
        }

        public Integer getFullWidth() {
            return fullWidth;
        }

        public void setFullWidth(Integer fullWidth) {
            this.fullWidth = fullWidth;
        }
    }
}
