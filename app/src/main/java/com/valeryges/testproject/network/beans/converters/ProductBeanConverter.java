package com.valeryges.testproject.network.beans.converters;


import android.support.annotation.Nullable;

import com.valeryges.testproject.models.IProduct;
import com.valeryges.testproject.models.Product;
import com.valeryges.testproject.models.converters.BaseConverter;
import com.valeryges.testproject.network.beans.ProductBean;

public class ProductBeanConverter extends BaseConverter<ProductBean, IProduct>
        implements IProductBeanConverter {


    @Override
    public IProduct convertINtoOUT(ProductBean inObject) {
        return convertINtoOUT(inObject, null);
    }


    @Override
    public IProduct convertINtoOUT(ProductBean inObject, @Nullable Object payload) {
        Product product = payload instanceof Product ? (Product) payload : new Product();
        product.setServerId(inObject.getListingId());
        product.setCategoryId(inObject.getCategoryId());
        product.setTitle(inObject.getTitle());
        product.setDescription(inObject.getDescription());
        product.setPrice(inObject.getPrice());
        product.setCurrencyCode(inObject.getCurrencyCode());
        ProductBean.MainImageBean mainImageBean = inObject.getMainImage();
        if (mainImageBean != null) {
            product.setUrl75x75(mainImageBean.getUrl75x75());
            product.setUrl170x135(mainImageBean.getUrl170x135());
            product.setUrl570xN(mainImageBean.getUrl570xN());
            product.setUrlFullxfull(mainImageBean.getUrlFullxfull());
        }
        return product;
    }

    @Override
    public ProductBean convertOUTtoIN(IProduct outObject) {
        return convertOUTtoIN(outObject, null);
    }

    @Override
    public ProductBean convertOUTtoIN(IProduct inObject, @Nullable Object payload) {
        throw new UnsupportedOperationException();
    }
}
