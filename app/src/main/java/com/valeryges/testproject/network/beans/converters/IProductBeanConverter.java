package com.valeryges.testproject.network.beans.converters;


import com.valeryges.testproject.models.IProduct;
import com.valeryges.testproject.models.converters.IConverter;
import com.valeryges.testproject.network.beans.ProductBean;

/**
 * Converter for Network entity {@link IProduct}. Extends {@link IConverter}
 */
public interface IProductBeanConverter extends IConverter<ProductBean, IProduct> {

}
