package com.valeryges.testproject.network.api;


import com.valeryges.testproject.network.responces.ProductResponse;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Products interface for retrofit
 */
public interface IProductsApi {
    @GET("listings/active")
    Observable<ProductResponse> getProducts(@Query("api_key") String apikey,
                                            @Query("category") String category,
                                            @Query("keywords") String keywords,
                                            @Query("limit") int limit,
                                            @Query("offset") int offset,
                                            @Query("includes") String includes);
}
