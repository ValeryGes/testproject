package com.valeryges.testproject.network.modules;

import com.valeryges.testproject.models.ICategory;

import java.util.List;

import rx.Observable;

/**
 * Encapsulates all operation with Category's requests to server side
 */
public interface ICategoriesModule {
    /**
     * Returns all existing top categories
     *
     * @return List of categories. {@link ICategory}
     */
    Observable<List<ICategory>> getCategories();
}
