package com.valeryges.testproject.network.modules;

/**
 * Base RxModule for wrapping Network realization of module for some entity
 *
 * @param <T> Type of Network interface
 */
public class BaseRxModule<T> {
    T module;

    public BaseRxModule(T module) {
        this.module = module;
    }
}
