package com.valeryges.testproject.network.modules;

import android.support.v4.util.Pair;

import com.valeryges.testproject.models.ICategory;
import com.valeryges.testproject.models.IProduct;

import java.util.List;

import rx.Observable;

/**
 * Encapsulates all operation with Product's requests to server side
 */
public interface IProductsModule {

    /**
     * Searches and returns products with next params
     *
     * @param category Instance of {@link ICategory}
     * @param keywords Keywords for search
     * @param limit    Limit for pagination
     * @param offset   Offset for pagination
     * @return List of products {@link IProduct}
     */
    Observable<List<IProduct>> getProducts(ICategory category,
                                           String keywords,
                                           int limit,
                                           int offset);

    /**
     * Search and return products with next params
     *
     * @param category Instance of {@link ICategory}
     * @param keywords Keywords for search
     * @param limit    Limit for pagination
     * @param offset   Offset for pagination
     * @return Pair with List of products {@link IProduct} and count of all products
     * user can get with this request
     */
    Observable<Pair<List<IProduct>, Integer>> getProductsWithCount(ICategory category,
                                                                   String keywords,
                                                                   int limit,
                                                                   int offset);
}
