package com.valeryges.testproject.network.beans.converters;


import com.valeryges.testproject.models.ICategory;
import com.valeryges.testproject.models.converters.IConverter;
import com.valeryges.testproject.network.beans.CategoryBean;

/**
 * Converter for Network entity {@link ICategory}. Extends {@link IConverter}
 */
public interface ICategoryBeanConverter extends IConverter<CategoryBean, ICategory> {
}
