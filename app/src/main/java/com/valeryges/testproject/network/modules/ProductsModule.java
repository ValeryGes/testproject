package com.valeryges.testproject.network.modules;

import android.support.v4.util.Pair;

import com.valeryges.testproject.BuildConfig;
import com.valeryges.testproject.TestProjectApplication;
import com.valeryges.testproject.models.ICategory;
import com.valeryges.testproject.models.IProduct;
import com.valeryges.testproject.network.api.IProductsApi;
import com.valeryges.testproject.network.beans.converters.IProductBeanConverter;
import com.valeryges.testproject.network.responces.ProductResponse;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

public class ProductsModule extends BaseRxModule<IProductsApi> implements IProductsModule {

    private static final String INCLUDES = "MainImage";
    @Inject
    IProductBeanConverter productBeanConverter;

    public ProductsModule(IProductsApi module) {
        super(module);
        TestProjectApplication.getAppComponent().inject(this);
    }

    @Override
    public Observable<List<IProduct>> getProducts(ICategory category,
                                                  String keywords,
                                                  int limit,
                                                  int offset) {
        return module
                .getProducts(BuildConfig.ACCESS_TOKEN, category.getName(),
                        keywords, limit, offset, INCLUDES)
                .map(ProductResponse::getResults)
                .compose(productBeanConverter.listOUTtoIN());
    }


    @Override
    public Observable<Pair<List<IProduct>, Integer>> getProductsWithCount(ICategory category,
                                                                          String keywords,
                                                                          int limit,
                                                                          int offset) {
        return module
                .getProducts(BuildConfig.ACCESS_TOKEN, category.getName(),
                        keywords, limit, offset, INCLUDES)
                .map(productResponce -> {
                    Integer count = productResponce.getCount();
                    List<IProduct> products =
                            productBeanConverter.convertListInToOut(productResponce.getResults());
                    return new Pair<>(products, count);
                });
    }
}
