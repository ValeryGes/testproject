package com.valeryges.testproject.network.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Generated Network bean for Category
 */
public class CategoryBean implements Serializable {

    @Expose
    @SerializedName("category_id")
    private Long categoryId;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("meta_title")
    private String metaTitle;
    @Expose
    @SerializedName("meta_keywords")
    private String metaKeywords;
    @Expose
    @SerializedName("meta_description")
    private String metaDescription;
    @Expose
    @SerializedName("page_description")
    private String pageDescription;
    @Expose
    @SerializedName("page_title")
    private String pageTitle;
    @Expose
    @SerializedName("category_name")
    private String categoryName;
    @Expose
    @SerializedName("short_name")
    private String shortName;
    @Expose
    @SerializedName("long_name")
    private String longName;
    @Expose
    @SerializedName("num_children")
    private Integer numChildren;

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMetaTitle() {
        return metaTitle;
    }

    public void setMetaTitle(String metaTitle) {
        this.metaTitle = metaTitle;
    }

    public String getMetaKeywords() {
        return metaKeywords;
    }

    public void setMetaKeywords(String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public String getPageDescription() {
        return pageDescription;
    }

    public void setPageDescription(String pageDescription) {
        this.pageDescription = pageDescription;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public Integer getNumChildren() {
        return numChildren;
    }

    public void setNumChildren(Integer numChildren) {
        this.numChildren = numChildren;
    }
}
