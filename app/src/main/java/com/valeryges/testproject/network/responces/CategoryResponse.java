package com.valeryges.testproject.network.responces;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.valeryges.testproject.network.api.ICategoriesApi;
import com.valeryges.testproject.network.beans.CategoryBean;

import java.io.Serializable;
import java.util.List;

/**
 * Generated entity of response for {@link ICategoriesApi#findAllTopCategory(String)}
 */
public class CategoryResponse implements Serializable {


    @Expose
    @SerializedName("count")
    private Integer count;
    @Expose
    @SerializedName("params")
    private Object params;
    @Expose
    @SerializedName("type")
    private String type;
    @Expose
    @SerializedName("results")
    private List<CategoryBean> results;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Object getParams() {
        return params;
    }

    public void setParams(Object params) {
        this.params = params;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public List<CategoryBean> getResults() {
        return results;
    }

    public void setResults(List<CategoryBean> results) {
        this.results = results;
    }

}
