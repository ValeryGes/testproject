package com.valeryges.testproject.network.api;


import com.valeryges.testproject.network.responces.CategoryResponse;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Categories interface for retrofit
 */
public interface ICategoriesApi {
    @GET("taxonomy/categories")
    Observable<CategoryResponse> findAllTopCategory(@Query("api_key") String apikey);
}
