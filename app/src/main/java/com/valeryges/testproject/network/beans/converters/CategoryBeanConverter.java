package com.valeryges.testproject.network.beans.converters;


import android.support.annotation.Nullable;

import com.valeryges.testproject.models.Category;
import com.valeryges.testproject.models.ICategory;
import com.valeryges.testproject.models.converters.BaseConverter;
import com.valeryges.testproject.network.beans.CategoryBean;

public final class CategoryBeanConverter extends BaseConverter<CategoryBean, ICategory>
        implements ICategoryBeanConverter{

    @Override
    public ICategory convertINtoOUT(CategoryBean inObject) {
        return convertINtoOUT(inObject, null);
    }

    @Override
    public ICategory convertINtoOUT(CategoryBean inObject, @Nullable Object payload) {
        Category out = payload instanceof Category ? (Category) payload : new Category();
        out.setServerId(inObject.getCategoryId());
        out.setName(inObject.getName());
        out.setLongName(inObject.getLongName());
        return out;
    }

    @Override
    public CategoryBean convertOUTtoIN(ICategory outObject) {
        return convertOUTtoIN(outObject, null);
    }

    @Override
    public CategoryBean convertOUTtoIN(ICategory outObject, @Nullable Object payload) {
        CategoryBean categoryBean = payload instanceof CategoryBean
                ? (CategoryBean) payload
                : new CategoryBean();
        categoryBean.setCategoryId(outObject.getServerId());
        categoryBean.setName(outObject.getName());
        categoryBean.setLongName(outObject.getLongName());
        return categoryBean;
    }
}
