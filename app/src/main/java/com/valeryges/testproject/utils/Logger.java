package com.valeryges.testproject.utils;

import android.support.annotation.Nullable;
import android.util.Log;

import com.valeryges.testproject.BuildConfig;

/**
 * Util with methods duplicates {@link Log}
 * But it will print logs only in debug builds
 */
@SuppressWarnings("WeakerAccess")
public final class Logger {

    private Logger() {
    }

    public static void e(String tag, String str) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, str);
        }
    }

    public static void e(String tag, String str, Throwable throwable) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, str, throwable);
        }
    }

    public static void i(String tag, String str) {
        if (BuildConfig.DEBUG) {
            Log.i(tag, str);
        }
    }

    public static void d(String tag, String str) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, str);
        }
    }

    public static void printStackTrace(@Nullable Throwable throwable) {
        if (BuildConfig.DEBUG && throwable != null) {
            throwable.printStackTrace();
        }
    }

}
