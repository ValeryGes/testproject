package com.valeryges.testproject.injections.components;


import com.valeryges.testproject.database.repositories.CategoryRepository;
import com.valeryges.testproject.database.repositories.ProductRepository;
import com.valeryges.testproject.injections.modules.AppModule;
import com.valeryges.testproject.injections.modules.DataBaseModule;
import com.valeryges.testproject.injections.modules.NetworkModule;
import com.valeryges.testproject.network.modules.CategoriesModule;
import com.valeryges.testproject.network.modules.ProductsModule;
import com.valeryges.testproject.ui.fragments.list_fragment.ListPresenter;
import com.valeryges.testproject.ui.fragments.main_fragment.SavedProductsPresenter;
import com.valeryges.testproject.ui.fragments.main_fragment.SearchPresenter;
import com.valeryges.testproject.ui.fragments.product_fragment.ProductPresenter;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, DataBaseModule.class})
public interface AppComponent {

    void inject(SearchPresenter searchPresenter);

    void inject(CategoriesModule categoriesModule);

    void inject(CategoryRepository categoryRepository);

    void inject(ProductsModule productsModule);

    void inject(ProductRepository productRepository);

    void inject(ListPresenter listPresenter);

    void inject(ProductPresenter productPresenter);

    void inject(SavedProductsPresenter savedProductsPresenter);
}
