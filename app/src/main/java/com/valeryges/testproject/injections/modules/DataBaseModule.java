package com.valeryges.testproject.injections.modules;


import android.app.Application;

import com.valeryges.testproject.BuildConfig;
import com.valeryges.testproject.database.converters.CategoryDBConverter;
import com.valeryges.testproject.database.converters.ICategoryDBConverter;
import com.valeryges.testproject.database.converters.IProductDBConverter;
import com.valeryges.testproject.database.converters.ProductDBConverter;
import com.valeryges.testproject.database.dbmodels.CategoryDBDao;
import com.valeryges.testproject.database.dbmodels.DaoMaster;
import com.valeryges.testproject.database.dbmodels.DaoSession;
import com.valeryges.testproject.database.dbmodels.ProductDBDao;
import com.valeryges.testproject.database.repositories.CategoryRepository;
import com.valeryges.testproject.database.repositories.ICategoryRepository;
import com.valeryges.testproject.database.repositories.IProductRepository;
import com.valeryges.testproject.database.repositories.ProductRepository;

import org.greenrobot.greendao.database.Database;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DataBaseModule {

    @Inject
    Application application;

    @Singleton
    @Provides
    DaoSession provideDaoSession(Application application) {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(application, BuildConfig.DB_NAME);
        Database db = helper.getWritableDb();
        return new DaoMaster(db).newSession();
    }

    @Singleton
    @Provides
    CategoryDBDao provideCategoryDBDao(DaoSession daoSession) {
        return daoSession.getCategoryDBDao();
    }

    @Singleton
    @Provides
    ProductDBDao provideProductDBDao(DaoSession daoSession) {
        return daoSession.getProductDBDao();
    }

    @Singleton
    @Provides
    ICategoryDBConverter provideCategoryDBConverter() {
        return new CategoryDBConverter();
    }

    @Singleton
    @Provides
    IProductDBConverter provideProductDBConverter() {
        return new ProductDBConverter();
    }

    @Singleton
    @Provides
    ICategoryRepository provideCategoryRepository() {
        return new CategoryRepository();
    }

    @Singleton
    @Provides
    IProductRepository provideProductRepository() {
        return new ProductRepository();
    }

}
