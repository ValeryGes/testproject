package com.valeryges.testproject.injections.modules;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.valeryges.testproject.BuildConfig;
import com.valeryges.testproject.network.api.ICategoriesApi;
import com.valeryges.testproject.network.api.IProductsApi;
import com.valeryges.testproject.network.beans.converters.CategoryBeanConverter;
import com.valeryges.testproject.network.beans.converters.ICategoryBeanConverter;
import com.valeryges.testproject.network.beans.converters.IProductBeanConverter;
import com.valeryges.testproject.network.beans.converters.ProductBeanConverter;
import com.valeryges.testproject.network.modules.CategoriesModule;
import com.valeryges.testproject.network.modules.ICategoriesModule;
import com.valeryges.testproject.network.modules.IProductsModule;
import com.valeryges.testproject.network.modules.ProductsModule;

import java.lang.reflect.Modifier;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


@SuppressWarnings({"FieldCanBeLocal"})
@Module
public class NetworkModule {

    private static final String API_VERSION_V2 = "v2/";


    private static final String BASE_URL = BuildConfig.ENDPOINT + API_VERSION_V2;

    @Singleton
    @Provides
    Retrofit provideRetrofit() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        HttpLoggingInterceptor.Level logLevel = BuildConfig.DEBUG ?
                HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE;
        httpLoggingInterceptor.setLevel(logLevel);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder();

                    Request request = requestBuilder
                            .method(original.method(), original.body())
                            .build();

                    return chain.proceed(request);
                })
                .addInterceptor(httpLoggingInterceptor)
                .connectTimeout(50, TimeUnit.SECONDS)
                .readTimeout(50, TimeUnit.SECONDS)
                .writeTimeout(50, TimeUnit.SECONDS)
                .build();

        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);

        Gson gson = builder
                .setLenient()
                .create();


        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

    }

    @Singleton
    @Provides
    ICategoriesModule provideCategoriesModule(Retrofit retrofit) {
        return new CategoriesModule(retrofit.create(ICategoriesApi.class));
    }

    @Singleton
    @Provides
    IProductsModule provideProductsModule(Retrofit retrofit) {
        return new ProductsModule(retrofit.create(IProductsApi.class));
    }

    @Singleton
    @Provides
    ICategoryBeanConverter provideCategoryBeanConverter() {
        return new CategoryBeanConverter();
    }

    @Singleton
    @Provides
    IProductBeanConverter provideProductBeanConverter() {
        return new ProductBeanConverter();
    }


}