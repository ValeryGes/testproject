package com.valeryges.testproject.events;

import com.valeryges.testproject.utils.RxBus;

/**
 * Class for {@link RxBus} event.
 * It must be send when there is a reason to reload saved products from Database
 */
public final class ReloadSavedEvent {
}

