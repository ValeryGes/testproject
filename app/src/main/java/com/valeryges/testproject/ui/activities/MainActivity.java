package com.valeryges.testproject.ui.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.valeryges.testproject.R;
import com.valeryges.testproject.funcs.Func1;
import com.valeryges.testproject.models.ICategory;
import com.valeryges.testproject.models.IProduct;
import com.valeryges.testproject.ui.fragments.list_fragment.ListFragment;
import com.valeryges.testproject.ui.fragments.main_fragment.MainFragment;
import com.valeryges.testproject.ui.fragments.product_fragment.ProductFragment;
import com.valeryges.testproject.utils.KeyboardUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainActivity extends ProgressActivity {

    private static final String BACK = "backStack";

    @BindView(R.id.toolbarMain)
    Toolbar toolbarMain;


    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);
        initToolbar();
        if (savedInstanceState == null) {
            replace(MainFragment.newInstance(), false);
        }
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void setTitle(String title) {
        checkActionBarAndCall(actionBar -> actionBar.setTitle(title));
    }

    @Override
    public void search(String keywords, ICategory category) {
        replace(ListFragment.newInstance(keywords, category));
    }

    @Override
    public void showProductDetails(IProduct product) {
        replace(ProductFragment.newInstance(product));
    }

    @Override
    public void close() {
        finish();
    }

    private void initToolbar() {
        setSupportActionBar(toolbarMain);
        checkActionBarAndCall(actionBar -> actionBar.setDisplayHomeAsUpEnabled(true));
    }

    private void replace(Fragment fragment) {
        replace(fragment, true);
    }

    private void replace(Fragment fragment, boolean addToBackStack) {
        KeyboardUtils.hideKeyboard(this);
        String tag = fragment.getClass().getSimpleName();
        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.vContainer, fragment, tag);

        if (addToBackStack) {
            transaction.addToBackStack(tag + BACK);
        }
        transaction.commit();
    }

    private void checkActionBarAndCall(Func1<ActionBar> func1) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            func1.call(actionBar);
        }
    }

    @Override
    public void onBackPressed() {
        // just in case. Some devices really doesn't want to hide keyboard so easy :)
        KeyboardUtils.hideKeyboard(this);
        super.onBackPressed();
    }
}
