package com.valeryges.testproject.ui.fragments.list_fragment;

import android.app.Application;

import com.valeryges.testproject.R;
import com.valeryges.testproject.TestProjectApplication;
import com.valeryges.testproject.database.repositories.IProductRepository;
import com.valeryges.testproject.models.ICategory;
import com.valeryges.testproject.models.IProduct;
import com.valeryges.testproject.network.modules.IProductsModule;
import com.valeryges.testproject.ui.fragments.CommonPresenter;
import com.valeryges.testproject.utils.Logger;
import com.valeryges.testproject.utils.NetworkUtils;
import com.valeryges.testproject.utils.RxUtils;
import com.valeryges.testproject.utils.Transformers;

import java.util.List;

import javax.inject.Inject;

import rx.functions.Action1;

public class ListPresenter extends CommonPresenter<IListView> implements IListPresenter {

    @Inject
    Application application;

    @Inject
    IProductsModule productsModule;

    @Inject
    IProductRepository productRepository;

    private boolean isFirstStart = true;
    private ICategory category;
    private String keywords;
    private int limit = IListView.LIMIT;
    private int offset = 0;
    private int count;


    private final Action1<List<IProduct>> onFirstLoadSuccess = products -> {
        showHideProgress(false);
        checkAndCallOrAddToDelayed(listView -> listView.addFirstPortion(products, count));
    };

    private final Action1<Throwable> onFirstLoadError = throwable -> {
        showHideProgress(false);
        Logger.printStackTrace(throwable);
        checkAndCallOrAddToDelayed(listView -> listView.showError(throwable.getMessage()));
    };

    private final Action1<List<IProduct>> onNewPortionLoadSuccess = products -> {
        checkAndCallOrAddToDelayed(listView -> listView.addNewPortion(products, count));
    };

    private final Action1<Throwable> onNewPortionLoadError = throwable -> {
        Logger.printStackTrace(throwable);
        checkAndCallOrAddToDelayed(listView -> listView.showPaginationError(throwable.getMessage()));
    };

    ListPresenter() {
        super();
        TestProjectApplication.getAppComponent().inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isFirstStart) {
            keywords = checkAndCallWithResult(IListView::getKeywords);
            category = checkAndCallWithResult(IListView::getCategory);
            offset = 0;
            loadProducts(true, onFirstLoadSuccess, onFirstLoadError);
            isFirstStart = false;
        }
    }

    @Override
    public void reloadProducts() {
        RxUtils.unsubscribeIfNotNull(getCompositeSubscription());
        offset = 0;
        loadProducts(false, onFirstLoadSuccess, onFirstLoadError);
    }

    @Override
    public void loadNewPortion() {
        loadProducts(false, onNewPortionLoadSuccess, onNewPortionLoadError);
    }

    private void loadProducts(boolean withProgress,
                              Action1<List<IProduct>> onSuccess,
                              Action1<Throwable> onError) {
        if (NetworkUtils.isConnected()) {
            if (withProgress) {
                showHideProgress(true);
            }
            getCompositeSubscription().add(
                    productsModule
                            .getProductsWithCount(category, keywords, IListView.LIMIT, offset)
                            .compose(Transformers.loadAsync())
                            .map(pair -> {
                                count = pair.second;
                                offset = offset + limit;
                                return pair.first;
                            })
                            .compose(Transformers.loadAsync())
                            .subscribe(onSuccess, onError));
        } else {
            checkAndCallOrAddToDelayed(listView ->
                    listView.showError(application.getString(R.string.network_connection)));
        }
    }


}
