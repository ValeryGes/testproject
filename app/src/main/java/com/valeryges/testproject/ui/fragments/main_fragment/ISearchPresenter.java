package com.valeryges.testproject.ui.fragments.main_fragment;

import com.valeryges.testproject.mvp.IPresenter;

interface ISearchPresenter extends IPresenter<ISearchView> {
    void loadCategories();
}
