package com.valeryges.testproject.ui.fragments.main_fragment;

import com.valeryges.testproject.models.IProduct;
import com.valeryges.testproject.ui.fragments.IBaseView;

import java.util.List;

interface ISavedProductsView extends IBaseView<ISavedProductsPresenter> {
    void showError(String message);
    void setSavedProducts(List<IProduct> products);
    void productsRemoved(List<IProduct> products);
}
