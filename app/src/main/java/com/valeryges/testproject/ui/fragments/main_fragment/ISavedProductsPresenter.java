package com.valeryges.testproject.ui.fragments.main_fragment;

import com.valeryges.testproject.models.IProduct;
import com.valeryges.testproject.mvp.IPresenter;

import java.util.List;

interface ISavedProductsPresenter extends IPresenter<ISavedProductsView> {
    void deleteProducts(List<IProduct> products);
}
