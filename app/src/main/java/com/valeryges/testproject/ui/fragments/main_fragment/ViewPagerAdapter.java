package com.valeryges.testproject.ui.fragments.main_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;


class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private final Context context;
    private List<PageSettings> pages;

    ViewPagerAdapter(@NonNull FragmentManager fm,
                     @NonNull Context context,
                     @NonNull List<PageSettings> pages) {
        super(fm);
        this.context = context.getApplicationContext();
        this.pages = pages;
    }

    static PageSettings createPage(Class<? extends Fragment> fragmentClass,
                                   String title) {
        return createPage(fragmentClass, title, new Bundle());
    }

    static PageSettings createPage(@NonNull Class<? extends Fragment> fragmentClass,
                                   @NonNull String title,
                                   @NonNull Bundle args) {
        return new PageSettings(fragmentClass, title, args);
    }


    @Override
    public Fragment getItem(int position) {
        final PageSettings pageSettings = pages.get(position);
        return Fragment.instantiate(context,
                pageSettings.getFragmentClass().getName(),
                pageSettings.getArgs());
    }

    @Override
    public int getCount() {
        return pages.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return pages.get(position).getTitle();
    }

    static class PageSettings {
        private final Class<? extends Fragment> fragmentClass;
        private final String title;
        private final Bundle args;

        private PageSettings(@NonNull Class<? extends Fragment> fragmentClass,
                             @NonNull String title,
                             @NonNull Bundle args) {
            this.fragmentClass = fragmentClass;
            this.title = title;
            this.args = args;
        }

        public String getTitle() {
            return title;
        }

        public Class<? extends Fragment> getFragmentClass() {
            return fragmentClass;
        }

        @NonNull
        public Bundle getArgs() {
            return args;
        }
    }
}
