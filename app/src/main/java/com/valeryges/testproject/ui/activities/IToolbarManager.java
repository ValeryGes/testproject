package com.valeryges.testproject.ui.activities;


/**
 * Encapsulates working with toolbar
 */
public interface IToolbarManager {

    /**
     * Setter of toolbar title
     *
     * @param title new Title for toolbar
     */
    void setTitle(String title);
}
