package com.valeryges.testproject.ui.fragments;


final class NoImplementationException extends ClassCastException {

    NoImplementationException() {
    }

    NoImplementationException(String message) {
        super(message);
    }

}
