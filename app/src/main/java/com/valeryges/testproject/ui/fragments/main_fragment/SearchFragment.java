package com.valeryges.testproject.ui.fragments.main_fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.valeryges.testproject.R;
import com.valeryges.testproject.models.ICategory;
import com.valeryges.testproject.ui.LoaderContract;
import com.valeryges.testproject.ui.fragments.BaseFragment;
import com.valeryges.testproject.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class SearchFragment extends BaseFragment<ISearchPresenter> implements ISearchView {

    private static final String EXTRA_CATEGORIES = "extraCategories";
    private static final String EXTRA_IS_ALERT_SHOWN = "extraIsAlertShown";
    private static final String EXTRA_ALERT_MESSAGE = "extraAlertMessage";

    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.spCategories)
    Spinner spCategories;

    private Unbinder unbinder;

    private List<ICategory> categories;

    private AlertDialog alertDialog;
    private boolean isAlertShown;
    private String messageForAlert;

    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        initPresenter(LoaderContract.Fragments.SEARCH, this, SearchPresenter::new);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_search, container, false);
        unbinder = ButterKnife.bind(this, root);
        if (savedInstanceState != null) {
            categories = savedInstanceState.getParcelableArrayList(EXTRA_CATEGORIES);
            isAlertShown = savedInstanceState.getBoolean(EXTRA_IS_ALERT_SHOWN);
            messageForAlert = savedInstanceState.getString(EXTRA_ALERT_MESSAGE);
        }
        if (categories != null) {
            initCategories(categories);
        }
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(isAlertShown && messageForAlert != null) {
            showAlert(messageForAlert);
        }
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
        super.onDestroyView();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (categories != null) {
            ArrayList<ICategory> categoriesToSave =
                    categories instanceof ArrayList
                            ? (ArrayList<ICategory>) categories
                            : new ArrayList<>(categories);
            outState.putParcelableArrayList(EXTRA_CATEGORIES, categoriesToSave);
        }
        outState.putBoolean(EXTRA_IS_ALERT_SHOWN, isAlertShown);
        outState.putString(EXTRA_ALERT_MESSAGE, messageForAlert);
    }

    @Override
    public void initCategories(@NonNull List<ICategory> categories) {
        this.categories = categories;
        ArrayAdapter<ICategory> categoriesAdapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_1,
                categories);
        spCategories.setAdapter(categoriesAdapter);
    }

    @OnClick(R.id.bSubmit)
    void onClick(View view) {
        String productName = etSearch.getText().toString();
        if (TextUtils.isEmpty(productName)) {
            showError(getString(R.string.empty_product_field));
        } else if (!NetworkUtils.isConnected()) {
            showError(getString(R.string.network_connection));
        } else {
            etSearch.setText(null);
            getMainManager().search(productName, (ICategory) spCategories.getSelectedItem());
        }
    }

    public void showAlert(String message) {
        isAlertShown = true;
        messageForAlert = message;
        alertDialog = new AlertDialog.Builder(getContext())
                .setMessage(message)
                .setPositiveButton(R.string.try_again, (dialog, which) -> {
                    alertDialog.dismiss();
                    loadCategories();
                    isAlertShown = false;
                })
                .setNegativeButton(R.string.close_app, ((dialog, which) -> {
                    isAlertShown = false;
                    getMainManager().close();
                }))
                .setCancelable(false)
                .show();
    }

    private void loadCategories() {
        ISearchPresenter presenter = getPresenter();
        if (presenter != null) {
            presenter.loadCategories();
        } else {
            showAlert(messageForAlert);
        }
    }

}
