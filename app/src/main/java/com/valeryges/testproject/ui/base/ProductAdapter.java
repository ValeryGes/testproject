package com.valeryges.testproject.ui.base;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.valeryges.testproject.R;
import com.valeryges.testproject.models.IProduct;
import com.valeryges.testproject.utils.ImageUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductAdapter extends BaseRecyclerViewAdapter<IProduct, ProductAdapter.ProductHolder>
        implements View.OnClickListener {

    private WeakReference<ProductAdapterListener> listenerWeakReference;
    private List<Integer> selectedPositions = new ArrayList<>();
    private boolean inSelectionMode;

    public ProductAdapter(@NonNull Context context,
                          @NonNull ProductAdapterListener listener,
                          @NonNull List<IProduct> data) {
        super(context, data);
        listenerWeakReference = new WeakReference<>(listener);
    }

    public ProductAdapter(@NonNull Context context, @NonNull ProductAdapterListener listener) {
        super(context);
        listenerWeakReference = new WeakReference<>(listener);
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return ProductHolder.newInstance(getInflater(), parent, this);
    }

    @Override
    public void onBindViewHolder(ProductHolder holder, int position) {
        holder.bind(getItem(position));
        holder.postBind(selectedPositions.contains(position));
    }

    @Override
    public void onBindViewHolder(ProductHolder holder, int position, List<Object> payloads) {
        if (payloads == null || payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads);
            return;
        }
        Object payload = payloads.get(0);
        if (payload == Boolean.class) {
            holder.postBind((Boolean) payload);
        } else {
            super.onBindViewHolder(holder, position, payloads);
        }
    }

    @Override
    public void onClick(View v) {
        Integer position = (int) v.getTag();
        if (inSelectionMode) {
            if (selectedPositions.contains(position)) {
                selectedPositions.remove(position);
                notifyItemChanged(position, Boolean.FALSE);
            } else {
                selectedPositions.add(position);
                notifyItemChanged(position, Boolean.TRUE);
            }
        } else {
            ProductAdapterListener listener = listenerWeakReference.get();
            if (listener != null) {
                listener.onProductSelected(getItem(position));
            }
        }

    }

    public List<Integer> getSelectedPositions() {
        return selectedPositions;
    }

    public void setSelectedPositions(List<Integer> selectedPositions) {
        if (selectedPositions == null) {
            this.selectedPositions.clear();
        } else {
            this.selectedPositions = selectedPositions;
        }
    }

    public List<IProduct> getSelectedProducts() {
        List<IProduct> selectedProducts = new ArrayList<>();
        for (Integer position : selectedPositions) {
            selectedProducts.add(data.get(position));
        }
        return selectedProducts;
    }

    public boolean isInSelectionMode() {
        return inSelectionMode;
    }

    public void setInSelectionMode(boolean inSelectionMode) {
        if (this.inSelectionMode == inSelectionMode) {
            return;
        }
        this.inSelectionMode = inSelectionMode;
        if (!inSelectionMode && selectedPositions != null && !selectedPositions.isEmpty()) {
            List<Integer> copy = new ArrayList<>(selectedPositions);
            selectedPositions.clear();
            for (Integer position : copy) {
                notifyItemChanged(position, Boolean.FALSE);
            }
        }
    }

    static class ProductHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivImage)
        ImageView ivImage;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.vSelectedBackground)
        View vSelectedBackground;
        @BindView(R.id.vSelect)
        View vSelect;

        private static ProductHolder newInstance(LayoutInflater inflater,
                                                 ViewGroup parent,
                                                 View.OnClickListener listener) {
            View itemView = inflater.inflate(R.layout.item_product, parent, false);
            return new ProductHolder(itemView, listener);
        }

        private ProductHolder(View itemView, View.OnClickListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            vSelect.setOnClickListener(listener);
        }

        private void bind(IProduct product) {
            ImageUtils.loadImage(ivImage, product.getUrl170x135(), R.drawable.ic_shopping_cart);
            tvTitle.setText(product.getTitle());
            vSelect.setTag(getAdapterPosition());
        }

        private void postBind(boolean selected) {
            vSelectedBackground.setVisibility(selected ? View.VISIBLE : View.GONE);
        }
    }

    public interface ProductAdapterListener {
        void onProductSelected(IProduct product);
    }
}
