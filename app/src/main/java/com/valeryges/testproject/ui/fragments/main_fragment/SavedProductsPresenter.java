package com.valeryges.testproject.ui.fragments.main_fragment;


import android.app.Application;

import com.valeryges.testproject.TestProjectApplication;
import com.valeryges.testproject.database.repositories.IProductRepository;
import com.valeryges.testproject.events.ReloadSavedEvent;
import com.valeryges.testproject.models.IProduct;
import com.valeryges.testproject.ui.fragments.CommonPresenter;
import com.valeryges.testproject.utils.Logger;
import com.valeryges.testproject.utils.RxBus;
import com.valeryges.testproject.utils.Transformers;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Action1;

public class SavedProductsPresenter extends CommonPresenter<ISavedProductsView>
        implements ISavedProductsPresenter {

    @Inject
    Application application;

    @Inject
    IProductRepository productRepository;

    private boolean needToLoad = true;

    private final Action1<List<IProduct>> onDeleteSuccess = products -> {
        showHideProgress(false);
        checkAndCallOrAddToDelayed(
                savedProductsView -> savedProductsView.productsRemoved(products));
    };

    private final Action1<Throwable> onDeleteError = throwable -> {
        showHideProgress(false);
        Logger.printStackTrace(throwable);
        checkAndCallOrAddToDelayed(
                savedProductsView -> savedProductsView.showError(throwable.getMessage()));
    };

    private final Action1<List<IProduct>> onGetSavedSuccess = products -> {
        checkAndCallOrAddToDelayed(savedView -> savedView.setSavedProducts(products));
    };

    private final Action1<Throwable> onGetSavedError = throwable -> {
        Logger.printStackTrace(throwable);
        checkAndCallOrAddToDelayed(savedView -> savedView.showError(throwable.getMessage()));
    };

    public SavedProductsPresenter() {
        super();
        TestProjectApplication.getAppComponent().inject(this);
    }

    @Override
    public void onPresenterCreated() {
        super.onPresenterCreated();
        getCompositeSubscription().add(
                RxBus
                        .instance()
                        .filter(ReloadSavedEvent.class)
                        .subscribe(reloadSavedEvent -> {
                            needToLoad = true;
                        }));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (needToLoad) {
            getAllSavedProducts();
            needToLoad = false;
        }
    }

    private void getAllSavedProducts() {
        getCompositeSubscription().add(
                productRepository
                        .getSavedProducts()
                        .compose(Transformers.loadAsync())
                        .subscribe(onGetSavedSuccess, onGetSavedError));
    }

    @Override
    public void deleteProducts(List<IProduct> products) {
        showHideProgress(true);
        getCompositeSubscription().add(
                Observable.just(products)
                        .map(products1 -> {
                            for (IProduct product : products1) {
                                product.setSaved(false);
                            }
                            return products1;
                        })
                        .flatMap(productRepository::save)
                        .compose(Transformers.loadAsync())
                        .subscribe(onDeleteSuccess, onDeleteError));
    }
}
