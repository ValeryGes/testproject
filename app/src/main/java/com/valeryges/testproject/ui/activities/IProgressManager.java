package com.valeryges.testproject.ui.activities;

/**
 * Encapsulates logic with showing progress dialog
 */
public interface IProgressManager {

    /**
     * Shows Progress dialog with message
     *
     * @param message Text to display
     */
    void showAnimatedProgress(String message);

    /**
     * Shows Progress dialog with
     */
    void showAnimatedLoadingProgress();

    /**
     * Hides progress dialog
     */
    void hideProgress();

    /**
     * Returns status of showing progress dialog
     *
     * @return true if showing or false in another case
     */
    boolean isProgressShowing();
}
