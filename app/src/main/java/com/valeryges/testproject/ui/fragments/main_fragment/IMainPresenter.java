package com.valeryges.testproject.ui.fragments.main_fragment;


import com.valeryges.testproject.mvp.IPresenter;

interface IMainPresenter extends IPresenter<IMainView> {
}
