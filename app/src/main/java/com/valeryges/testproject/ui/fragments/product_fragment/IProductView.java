package com.valeryges.testproject.ui.fragments.product_fragment;


import com.valeryges.testproject.models.IProduct;
import com.valeryges.testproject.ui.fragments.IBaseView;

interface IProductView extends IBaseView<IProductPresenter> {
    IProduct getProduct();
    void onCheckResult(IProduct product);
    void showError(String message);
}
