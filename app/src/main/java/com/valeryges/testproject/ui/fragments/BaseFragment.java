package com.valeryges.testproject.ui.fragments;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;

import com.valeryges.testproject.mvp.BaseLoaderFragmentView;
import com.valeryges.testproject.mvp.IPresenter;
import com.valeryges.testproject.ui.activities.IMainManager;
import com.valeryges.testproject.utils.SnackBarUtil;


public abstract class BaseFragment<T extends IPresenter> extends BaseLoaderFragmentView<T>
        implements IBaseView<T> {

    private IMainManager listener;
    private Snackbar snackbar;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Fragment parentFragment = getParentFragment();
        if (parentFragment instanceof IMainManager) {
            listener = (IMainManager) parentFragment;
        } else if (context instanceof IMainManager) {
            listener = (IMainManager) context;
        } else {
            throw new NoImplementationException("Context or parent fragment must implement "
                    + IMainManager.class.getSimpleName());
        }
    }

    @Override
    public void onDestroyView() {
        hideSnackBar();
        snackbar = null;
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        listener = null;
        super.onDetach();
    }


    public void showError(String message) {
        View view = getView();
        snackbar = SnackBarUtil.showErrorLong(view, message);
    }

    public void showSuccess(String message) {
        View view = getView();
        snackbar = SnackBarUtil.showSuccessLong(view, message);
    }

    public void showWarning(String message) {
        View view = getView();
        snackbar = SnackBarUtil.showWarningLong(view, message);
    }

    protected void hideSnackBar() {
        if (snackbar != null && snackbar.isShown()) {
            snackbar.dismiss();
        }
    }

    @Override
    public void showProgress() {
        getMainManager().showAnimatedLoadingProgress();
    }

    @Override
    public void hideProgress() {
        getMainManager().hideProgress();
    }

    protected IMainManager getMainManager() {
        return listener;
    }

}
