package com.valeryges.testproject.ui.fragments.main_fragment;

import com.valeryges.testproject.models.ICategory;
import com.valeryges.testproject.ui.fragments.IBaseView;

import java.util.List;

interface ISearchView extends IBaseView<ISearchPresenter> {
    void initCategories(List<ICategory> categories);
    void showAlert(String message);
}
