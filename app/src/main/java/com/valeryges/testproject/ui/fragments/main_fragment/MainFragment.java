package com.valeryges.testproject.ui.fragments.main_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.valeryges.testproject.R;
import com.valeryges.testproject.ui.LoaderContract;
import com.valeryges.testproject.ui.activities.IMainManager;
import com.valeryges.testproject.ui.fragments.BaseFragment;
import com.valeryges.testproject.utils.KeyboardUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class MainFragment extends BaseFragment<IMainPresenter>
        implements
        ViewPager.OnPageChangeListener,
        IMainView {

    @BindView(R.id.tabSelector)
    TabLayout tabSelector;
    @BindView(R.id.vpChooser)
    ViewPager vpChooser;


    private Unbinder unbinder;
    private List<ViewPagerAdapter.PageSettings> pages;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        initPresenter(LoaderContract.Fragments.MAIN, this, MainPresenter::new);
    }

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initPages();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);
        unbinder = ButterKnife.bind(this, root);
        initViewPager(pages);
        getMainManager().setTitle(pages.get(vpChooser.getCurrentItem()).getTitle());
        return root;
    }

    @Override
    public void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        // empty implementation
    }

    @Override
    public void onPageSelected(int position) {
        KeyboardUtils.hideKeyboard(this);
        IMainManager mainManager = getMainManager();
        if (mainManager != null) {
            mainManager.setTitle(pages.get(position).getTitle());
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        // empty implementation
    }

    private void initPages() {
        pages = new ArrayList<>();
        pages.add(ViewPagerAdapter.createPage(SearchFragment.class, getString(R.string.search)));
        pages.add(ViewPagerAdapter.createPage(SavedProductsFragment.class, getString(R.string.saved)));
    }

    private void initViewPager(List<ViewPagerAdapter.PageSettings> pages) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager(),
                getContext(),
                pages);
        vpChooser.setAdapter(adapter);
        tabSelector.setupWithViewPager(vpChooser);
        vpChooser.addOnPageChangeListener(this);
    }


}
