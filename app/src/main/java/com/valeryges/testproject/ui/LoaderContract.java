package com.valeryges.testproject.ui;

public final class LoaderContract {

    private LoaderContract() {
    }

    public static final class Activities {
        private Activities() {
        }

        public static final int MAIN = 0;
    }

    public static final class Fragments {
        private Fragments() {
        }

        public static final int MAIN = 1;
        public static final int SEARCH = 2;
        public static final int SAVED_PRODUCTS = 3;
        public static final int LIST = 4;
        public static final int DETAIL = 5;

    }
}
