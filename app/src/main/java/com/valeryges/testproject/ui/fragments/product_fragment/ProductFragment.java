package com.valeryges.testproject.ui.fragments.product_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.valeryges.testproject.R;
import com.valeryges.testproject.models.IProduct;
import com.valeryges.testproject.ui.LoaderContract;
import com.valeryges.testproject.ui.fragments.BaseFragment;
import com.valeryges.testproject.utils.ImageUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class ProductFragment extends BaseFragment<IProductPresenter> implements IProductView {

    private static final String EXTRA_PRODUCT = "extraProduct";
    private static final String EXTRA_ACTION = "extraAction";

    @BindView(R.id.ivPicture)
    ImageView ivPicture;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvPrice)
    TextView tvPrice;
    @BindView(R.id.tvDescription)
    TextView tvDescription;
    @BindView(R.id.fabSave)
    FloatingActionButton fabSave;
    @BindView(R.id.fabDelete)
    FloatingActionButton fabDelete;

    private Unbinder unbinder;

    private IProduct currentProduct;

    @AccessedActions
    private int currentAction;

    public static ProductFragment newInstance(IProduct product) {
        ProductFragment fragment = new ProductFragment();
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_PRODUCT, product);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        initPresenter(LoaderContract.Fragments.DETAIL, this, ProductPresenter::new);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            currentProduct = args.getParcelable(EXTRA_PRODUCT);
        }
        if (savedInstanceState != null) {
            @AccessedActions int action
                    = savedInstanceState.getInt(EXTRA_ACTION, AccessedActions.NO_ACTION);
            currentAction = action;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_product, container, false);
        unbinder = ButterKnife.bind(this, root);
        getMainManager().setTitle(getString(R.string.product));
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(currentProduct);
        invalidateFabs();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Bundle args = getArguments();
        // this extra is already in args, so there if we put it in outState Bundle
        // we will have extra entity in one of bundles and also extra checks in onCreate
        if (args != null) {
            args.putParcelable(EXTRA_PRODUCT, currentProduct);
        }
        outState.putInt(EXTRA_ACTION, currentAction);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public IProduct getProduct() {
        return currentProduct;
    }

    @Override
    public void onCheckResult(IProduct product) {
        currentProduct = product;
        invalidateAction(currentProduct);
    }

    @OnClick({R.id.fabSave, R.id.fabDelete})
    void onSaveClick(View view) {
        IProductPresenter presenter = getPresenter();
        if (presenter == null) {
            return;
        }
        int id = view.getId();
        if (id == R.id.fabSave) {
            presenter.storeProduct(currentProduct);
        } else if (id == R.id.fabDelete) {
            presenter.deleteProduct(currentProduct);
        }
    }

    private void init(IProduct product) {
        if (product == null) {
            return;
        }
        ImageUtils.loadImage(ivPicture, product.getUrlFullxfull(), R.drawable.ic_shopping_cart);
        tvTitle.setText(product.getTitle());
        tvPrice.setText(String.format("%s %s", product.getCurrencyCode(), product.getPrice()));
        tvDescription.setText(product.getDescription());
    }

    private void invalidateFabs() {
        if (currentAction == AccessedActions.NO_ACTION) {
            fabSave.hide();
            fabDelete.hide();
        } else if (currentAction == AccessedActions.ACTION_SAVE) {
            fabSave.show();
            fabDelete.hide();
        } else if (currentAction == AccessedActions.ACTION_DELETE) {
            fabSave.hide();
            fabDelete.show();
        }
    }

    private void invalidateAction(@NonNull IProduct product) {
        currentAction = product.isSaved()
                ? AccessedActions.ACTION_DELETE
                : AccessedActions.ACTION_SAVE;
        invalidateFabs();
    }

    @IntDef({AccessedActions.NO_ACTION, AccessedActions.ACTION_SAVE, AccessedActions.ACTION_DELETE})
    private @interface AccessedActions {
        int NO_ACTION = 0;
        int ACTION_SAVE = 1;
        int ACTION_DELETE = 2;
    }
}
