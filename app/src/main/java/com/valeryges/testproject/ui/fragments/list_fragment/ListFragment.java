package com.valeryges.testproject.ui.fragments.list_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.valeryges.testproject.R;
import com.valeryges.testproject.models.ICategory;
import com.valeryges.testproject.models.IProduct;
import com.valeryges.testproject.ui.LoaderContract;
import com.valeryges.testproject.ui.base.ProductAdapter;
import com.valeryges.testproject.ui.fragments.BaseFragment;
import com.valeryges.testproject.utils.NetworkUtils;
import com.valeryges.testproject.utils.OrientationUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class ListFragment extends BaseFragment<IListPresenter>
        implements IListView,
        ProductAdapter.ProductAdapterListener {

    private static final String EXTRA_KEYWORDS = "extraKeywords";
    private static final String EXTRA_CATEGORY = "extraCategory";

    private static final String EXTRA_CURRENT_PRODUCTS = "extraCurrentProducts";
    private static final String EXTRA_IS_REFRESHING = "extraIsRefreshing";
    private static final String EXTRA_PAGINATION_ENABLED = "extraPaginationEnabled";


    @BindView(R.id.srLayout)
    SwipeRefreshLayout srLayout;

    @BindView(R.id.rvLoadedProducts)
    RecyclerView rvLoadedProducts;

    private Unbinder unbinder;

    private ProductAdapter adapter;

    private String keywords;
    private ICategory currentCategory;

    private List<IProduct> currentProducts;
    private GridLayoutManager layoutManager;
    private boolean paginationEnabled;

    private final RecyclerView.OnScrollListener paginationListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int position = layoutManager.findLastVisibleItemPosition();
            int updatePosition = recyclerView.getAdapter().getItemCount() - 1 - (IListView.LIMIT / 2);
            if (position >= updatePosition) {
                requestNextPortion();
                recyclerView.removeOnScrollListener(this);
                paginationEnabled = false;
            }
        }
    };

    public static ListFragment newInstance(@NonNull String keywords, @NonNull ICategory category) {
        ListFragment fragment = new ListFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_KEYWORDS, keywords);
        args.putParcelable(EXTRA_CATEGORY, category);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        initPresenter(LoaderContract.Fragments.LIST, this, ListPresenter::new);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            keywords = args.getString(EXTRA_KEYWORDS);
            currentCategory = args.getParcelable(EXTRA_CATEGORY);
        }
        if (savedInstanceState != null) {
            currentProducts = savedInstanceState.getParcelableArrayList(EXTRA_CURRENT_PRODUCTS);
            paginationEnabled = savedInstanceState.getBoolean(EXTRA_PAGINATION_ENABLED);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list, container, false);
        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        boolean isRefreshing = false;
        if (savedInstanceState != null) {
            isRefreshing = savedInstanceState.getBoolean(EXTRA_IS_REFRESHING, false);
        }
        getMainManager().setTitle(getString(R.string.results));
        initRecyclerView();
        initSwipeToRefresh(isRefreshing);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (currentProducts != null) {
            ArrayList<IProduct> newProductsList = currentProducts instanceof ArrayList
                    ? (ArrayList<IProduct>) currentProducts
                    : new ArrayList<>(currentProducts);
            outState.putParcelableArrayList(EXTRA_CURRENT_PRODUCTS, newProductsList);
            outState.putBoolean(EXTRA_PAGINATION_ENABLED, paginationEnabled);
        }
        if (srLayout != null) {
            outState.putBoolean(EXTRA_IS_REFRESHING, srLayout.isRefreshing());
        }
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onProductSelected(IProduct product) {
        getMainManager().showProductDetails(product);
    }

    @Override
    public String getKeywords() {
        return keywords;
    }

    @Override
    public ICategory getCategory() {
        return currentCategory;
    }

    @Override
    public void addFirstPortion(List<IProduct> products, int count) {
        srLayout.setRefreshing(false);
        if (products != null && !products.isEmpty()) {
            currentProducts = products;
            adapter.clear();
            adapter.addAll(products);
            adapter.notifyDataSetChanged();
            if (currentProducts.size() < count) {
                enablePagination();
            }

        } else {
            showError(getString(R.string.no_results));
        }

    }

    @Override
    public void addNewPortion(List<IProduct> products, int count) {
        srLayout.setRefreshing(false);
        if (currentProducts == null) {
            currentProducts = new ArrayList<>();
        }
        currentProducts.addAll(products);
        adapter.addAll(products);
        adapter.notifyItemRangeInserted(adapter.getItemCount() - products.size(), products.size());
        if (currentProducts.size() < count) {
            enablePagination();
        }
    }

    @Override
    public void showError(String message) {
        srLayout.setRefreshing(false);
        super.showError(message);
    }

    @Override
    public void showPaginationError(String message) {
        enablePagination();
        showError(message);
    }

    private void initRecyclerView() {
        if (currentProducts != null && !currentProducts.isEmpty()) {
            adapter = new ProductAdapter(getContext(), this, currentProducts);
        } else {
            adapter = new ProductAdapter(getContext(), this);
        }
        rvLoadedProducts.setAdapter(adapter);
        int spanCount = OrientationUtils.isPortrait(this) ? 3 : 5;
        layoutManager = new GridLayoutManager(getContext(), spanCount,
                GridLayoutManager.VERTICAL, false);
        rvLoadedProducts.setLayoutManager(layoutManager);
        if (paginationEnabled) {
            enablePagination();
        }
    }

    private void initSwipeToRefresh(boolean isRefreshing) {
        srLayout.setRefreshing(isRefreshing);
        srLayout.setOnRefreshListener(this::requestReload);
    }

    private void requestReload() {
        IListPresenter presenter = getPresenter();
        if (presenter != null) {
            presenter.reloadProducts();
        } else {
            // in this case presenter can't be null but just in case
            srLayout.setRefreshing(false);
        }
    }

    private void requestNextPortion() {
        if (NetworkUtils.isConnected()) {
            IListPresenter presenter = getPresenter();
            if (presenter != null) {
                presenter.loadNewPortion();
            } else {
                // in this case presenter can't be null but just in case
                enablePagination();
            }
        } else {
            showError(getString(R.string.network_connection));
            enablePagination();
        }
    }

    private void enablePagination() {
        rvLoadedProducts.addOnScrollListener(paginationListener);
        paginationEnabled = true;
    }
}
