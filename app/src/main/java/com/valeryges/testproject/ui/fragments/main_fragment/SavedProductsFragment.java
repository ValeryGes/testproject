package com.valeryges.testproject.ui.fragments.main_fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.valeryges.testproject.R;
import com.valeryges.testproject.models.IProduct;
import com.valeryges.testproject.ui.LoaderContract;
import com.valeryges.testproject.ui.base.ProductAdapter;
import com.valeryges.testproject.ui.fragments.BaseFragment;
import com.valeryges.testproject.utils.OrientationUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SavedProductsFragment extends BaseFragment<ISavedProductsPresenter>
        implements
        ISavedProductsView,
        ProductAdapter.ProductAdapterListener {

    private static final String EXTRA_SAVED_PRODUCTS = "extraSavedProducts";
    private static final String EXTRA_CURRENT_ACTION = "extraCurrentActions";
    private static final String EXTRA_SELECTED_POSITIONS = "extraSelectionPositions";

    @BindView(R.id.rvSavedProducts)
    RecyclerView rvSavedProducts;
    @BindView(R.id.tvNoSavedProducts)
    TextView tvNoSavedProducts;
    @BindView(R.id.fabDelete)
    FloatingActionButton fabDelete;
    @BindView(R.id.fabConfirm)
    FloatingActionButton fabConfirm;
    @BindView(R.id.fabCancel)
    FloatingActionButton fabCancel;

    Unbinder unbinder;

    private List<IProduct> savedProducts;

    private ProductAdapter adapter;

    @AccessedActions
    private int currentAction;

    private AlertDialog alertDialog;

    public static SavedProductsFragment newInstance() {
        SavedProductsFragment fragment = new SavedProductsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        initPresenter(LoaderContract.Fragments.SAVED_PRODUCTS, this, SavedProductsPresenter::new);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            savedProducts = savedInstanceState.getParcelableArrayList(EXTRA_SAVED_PRODUCTS);
            @AccessedActions int action
                    = savedInstanceState.getInt(EXTRA_CURRENT_ACTION, AccessedActions.NO_ACTION);
            currentAction = action;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_saved_products, container, false);
        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        List<Integer> selectedPositions = null;
        if (savedInstanceState != null) {
            selectedPositions = (List<Integer>) savedInstanceState.getSerializable(EXTRA_SELECTED_POSITIONS);
        }
        initRecyclerView(savedProducts, selectedPositions);
        invalidateFabs();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (savedProducts != null) {
            ArrayList<IProduct> newProductsList = savedProducts instanceof ArrayList
                    ? (ArrayList<IProduct>) savedProducts
                    : new ArrayList<>(savedProducts);
            outState.putParcelableArrayList(EXTRA_SAVED_PRODUCTS, newProductsList);
        }
        outState.putInt(EXTRA_CURRENT_ACTION, currentAction);
        if (adapter != null) {
            ArrayList<Integer> selectedPositions = new ArrayList<>(adapter.getSelectedPositions());
            outState.putSerializable(EXTRA_SELECTED_POSITIONS, selectedPositions);
        }
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
        super.onDestroyView();
    }

    @Override
    public void onProductSelected(IProduct product) {
        getMainManager().showProductDetails(product);
    }

    @Override
    public void setSavedProducts(List<IProduct> products) {
        savedProducts = products;
        adapter.setInSelectionMode(false);
        adapter.clear();
        if (savedProducts != null && !savedProducts.isEmpty()) {
            adapter.addAll(savedProducts);
            currentAction = AccessedActions.ACTION_DELETE;
        } else {
            currentAction = AccessedActions.NO_ACTION;
        }
        adapter.notifyDataSetChanged();
        invalidateMessage();
        invalidateFabs();
    }

    @OnClick({R.id.fabDelete, R.id.fabConfirm, R.id.fabCancel})
    void onClickFab(View view) {
        int id = view.getId();
        if (id == R.id.fabDelete) {
            startSelectionMode();
        } else if (id == R.id.fabConfirm) {
            startConfirm();
        } else if (id == R.id.fabCancel) {
            stopSelectionMode();
        }
    }

    @Override
    public void productsRemoved(List<IProduct> products) {
        savedProducts.removeAll(products);
        for (IProduct product : products) {
            int position = adapter.getItemPosition(product);
            adapter.remove(position);
            adapter.notifyItemRemoved(position);
        }
        stopSelectionMode();
    }

    private void startSelectionMode() {
        currentAction = AccessedActions.ACTION_SELECTION;
        adapter.setInSelectionMode(true);
        invalidateFabs();
    }

    private void stopSelectionMode() {

        currentAction = (savedProducts != null && !savedProducts.isEmpty())
                ? AccessedActions.ACTION_DELETE
                : AccessedActions.NO_ACTION;
        adapter.setInSelectionMode(false);
        invalidateFabs();
    }

    private void startConfirm() {
        List<IProduct> selectedProducts = adapter.getSelectedProducts();
        if (selectedProducts == null || selectedProducts.isEmpty()) {
            showError(getString(R.string.need_to_select));
        } else {
            alertDialog = new AlertDialog.Builder(getContext())
                    .setMessage(getString(R.string.are_you_sure))
                    .setPositiveButton(R.string.yes, (dialog, which) -> {
                        alertDialog.dismiss();
                        removeProducts(selectedProducts);
                    })
                    .setNegativeButton(R.string.no, (dialog, which) -> {
                        alertDialog.dismiss();
                    })
                    .show();
        }

    }

    private void removeProducts(List<IProduct> products) {
        ISavedProductsPresenter presenter = getPresenter();
        if (presenter != null) {
            presenter.deleteProducts(products);
        } else {
            // just in case
            startConfirm();
        }
    }

    private void initRecyclerView(@Nullable List<IProduct> currentProducts,
                                  @Nullable List<Integer> selectedPositions) {
        if (currentProducts != null && !currentProducts.isEmpty()) {
            adapter = new ProductAdapter(getContext(), this, currentProducts);
        } else {
            adapter = new ProductAdapter(getContext(), this);
        }
        adapter.setInSelectionMode(currentAction == AccessedActions.ACTION_SELECTION);
        adapter.setSelectedPositions(selectedPositions);
        rvSavedProducts.setAdapter(adapter);
        int spanCount = OrientationUtils.isPortrait(this) ? 3 : 5;
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), spanCount,
                GridLayoutManager.VERTICAL, false);
        rvSavedProducts.setLayoutManager(layoutManager);
        invalidateMessage();
    }

    private void invalidateMessage() {
        if (savedProducts != null && !savedProducts.isEmpty()) {
            tvNoSavedProducts.setVisibility(View.GONE);
        } else {
            tvNoSavedProducts.setVisibility(View.VISIBLE);
        }
    }

    private void invalidateFabs() {
        if (currentAction == AccessedActions.NO_ACTION) {
            fabDelete.hide();
            fabConfirm.hide();
            fabCancel.hide();
        } else if (currentAction == AccessedActions.ACTION_DELETE) {
            fabDelete.show();
            fabConfirm.hide();
            fabCancel.hide();
        } else if (currentAction == AccessedActions.ACTION_SELECTION) {
            fabDelete.hide();
            fabConfirm.show();
            fabCancel.show();
        }
    }

    @IntDef({AccessedActions.NO_ACTION, AccessedActions.ACTION_DELETE, AccessedActions.ACTION_SELECTION})
    private @interface AccessedActions {
        int NO_ACTION = 0;
        int ACTION_DELETE = 1;
        int ACTION_SELECTION = 2;
    }


}
