package com.valeryges.testproject.ui.fragments.list_fragment;

import com.valeryges.testproject.mvp.IPresenter;

interface IListPresenter extends IPresenter<IListView> {
    void reloadProducts();
    void loadNewPortion();
}
