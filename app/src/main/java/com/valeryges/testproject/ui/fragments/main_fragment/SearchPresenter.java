package com.valeryges.testproject.ui.fragments.main_fragment;

import android.app.Application;

import com.valeryges.testproject.R;
import com.valeryges.testproject.TestProjectApplication;
import com.valeryges.testproject.database.repositories.ICategoryRepository;
import com.valeryges.testproject.models.ICategory;
import com.valeryges.testproject.network.modules.ICategoriesModule;
import com.valeryges.testproject.ui.fragments.CommonPresenter;
import com.valeryges.testproject.utils.Logger;
import com.valeryges.testproject.utils.NetworkUtils;
import com.valeryges.testproject.utils.Transformers;

import java.util.List;

import javax.inject.Inject;

import rx.functions.Action1;

public class SearchPresenter extends CommonPresenter<ISearchView> implements ISearchPresenter {

    @Inject
    Application application;
    @Inject
    ICategoriesModule categoriesModule;
    @Inject
    ICategoryRepository categoryRepository;

    private boolean isFirstStart = true;


    private final Action1<List<ICategory>> onCategoryLoadSuccess = categories -> {
        showHideProgress(false);
        if (!categories.isEmpty()) {
            checkAndCallOrAddToDelayed(searchView -> searchView.initCategories(categories));
        } else {
            checkAndCallOrAddToDelayed(view -> view.showAlert(application.getString(R.string.something_went_wrong)));
        }
    };

    private final Action1<Throwable> onErrorOnlineLoading = throwable -> {
        Logger.printStackTrace(throwable);
        loadCategoriesLocal();
    };

    private final Action1<Throwable> onErrorLocalLoading = throwable -> {
        showHideProgress(false);
        Logger.printStackTrace(throwable);
        checkAndCallOrAddToDelayed(view -> view.showAlert(application.getString(R.string.something_went_wrong)));
    };

    SearchPresenter() {
        super();
        TestProjectApplication.getAppComponent().inject(this);
    }


    @Override
    public void onResume() {
        super.onResume();
        if (isFirstStart) {
            loadCategories();
            isFirstStart = false;
        }
    }

    public void loadCategories() {
        showHideProgress(true);
        if (NetworkUtils.isConnected()) {
            loadCategoriesFromServer();
        } else {
            loadCategoriesLocal();
        }
    }

    private void loadCategoriesFromServer() {
        getCompositeSubscription().add(
                categoriesModule
                        .getCategories()
                        .flatMap(categoryRepository::save)
                        .compose(Transformers.loadAsync())
                        .subscribe(onCategoryLoadSuccess, onErrorOnlineLoading));
    }

    private void loadCategoriesLocal() {
        getCompositeSubscription().add(
                categoryRepository
                        .getAll()
                        .compose(Transformers.loadAsync())
                        .subscribe(onCategoryLoadSuccess, onErrorLocalLoading));
    }



}
