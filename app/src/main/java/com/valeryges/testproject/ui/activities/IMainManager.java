package com.valeryges.testproject.ui.activities;


import com.valeryges.testproject.models.ICategory;
import com.valeryges.testproject.models.IProduct;

/**
 * Interface for Main operations in application
 */
public interface IMainManager extends IToolbarManager, IProgressManager {

    /**
     * Makes Search and opens screen with results
     *
     * @param keywords for search
     * @param category Instance of {@link ICategory} for search
     */
    void search(String keywords, ICategory category);

    /**
     * Closes application
     */
    void close();

    /**
     * Shows detailed screen for product
     *
     * @param product Instance of {@link IProduct}
     */
    void showProductDetails(IProduct product);
}
