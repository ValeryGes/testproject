package com.valeryges.testproject.ui.fragments;


import com.valeryges.testproject.mvp.IPresenter;
import com.valeryges.testproject.mvp.IView;

public interface IBaseView<TPresenter extends IPresenter> extends IView<TPresenter> {
    void showProgress();
    void hideProgress();
}
