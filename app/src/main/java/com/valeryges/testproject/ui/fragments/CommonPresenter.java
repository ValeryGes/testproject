package com.valeryges.testproject.ui.fragments;

import android.support.annotation.Nullable;

import com.valeryges.testproject.funcs.Func1;
import com.valeryges.testproject.funcs.Func3;
import com.valeryges.testproject.mvp.BasePresenter;
import com.valeryges.testproject.utils.RxUtils;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import rx.subscriptions.CompositeSubscription;


public abstract class CommonPresenter<TView extends IBaseView> extends BasePresenter<TView> {

    private CompositeSubscription compositeSubscription;
    private Queue<Func1<TView>> delayedFuncs;
    private boolean isProgressShowing;

    @Override
    public void onResume() {
        super.onResume();
        if(isProgressShowing) {
            showHideProgress(true);
        }
        TView view = getView();
        if (view != null && !delayedFuncs.isEmpty()) {
            while (delayedFuncs.peek() != null) {
                delayedFuncs.poll().call(view);
            }
        }
    }

    @Override
    public void onPresenterCreated() {
        delayedFuncs = new ConcurrentLinkedQueue<>();
        super.onPresenterCreated();
    }

    @Override
    public void onPresenterDestroyed() {
        RxUtils.unsubscribeIfNotNull(compositeSubscription);
        delayedFuncs.clear();
        super.onPresenterDestroyed();
    }

    protected void checkAndCall(Func1<TView> func1) {
        TView view = getView();
        if (view != null) {
            func1.call(view);
        }
    }

    protected void checkAndCallOrAddToDelayed(Func1<TView> func1) {
        TView view = getView();
        if (view != null) {
            func1.call(view);
        } else {
            delayedFuncs.add(func1);
        }
    }

    @Nullable
    protected <R> R checkAndCallWithResult(Func3<TView, R> func1) {
        TView view = getView();
        if (view != null) {
            return func1.call(view);
        } else {
            return null;
        }
    }

    protected CompositeSubscription getCompositeSubscription() {
        compositeSubscription = RxUtils.getNewCompositeSubIfUnsubscribed(compositeSubscription);
        return compositeSubscription;
    }

    protected void showHideProgress(boolean needToShow) {
        isProgressShowing = needToShow;
        Func1<TView> func = needToShow
                ? TView::showProgress
                : TView::hideProgress;
        checkAndCall(func);
    }
}
