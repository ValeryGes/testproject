package com.valeryges.testproject.ui.fragments.list_fragment;

import com.valeryges.testproject.models.ICategory;
import com.valeryges.testproject.models.IProduct;
import com.valeryges.testproject.ui.fragments.IBaseView;

import java.util.List;

interface IListView extends IBaseView<IListPresenter> {
    int LIMIT = 25;
    String getKeywords();
    ICategory getCategory();
    void addFirstPortion(List<IProduct> products, int count);
    void addNewPortion(List<IProduct> products, int count);
    void showError(String message);
    void showPaginationError(String message);
}
