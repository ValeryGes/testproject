package com.valeryges.testproject.ui.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.valeryges.testproject.R;
import com.valeryges.testproject.ui.widgets.CirclesProgressDialog;


public abstract class ProgressActivity extends AppCompatActivity implements IMainManager {

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
    }


    @Override
    protected void onDestroy() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
        super.onDestroy();
    }

    @Override
    public void showAnimatedProgress(String message) {
        if (progressDialog != null) {
            progressDialog.show();
            progressDialog.setContentView(R.layout.animated_progress);
            ((CirclesProgressDialog) progressDialog.findViewById(R.id.progressDialog)).setText(message);
        }
    }

    @Override
    public void showAnimatedLoadingProgress() {//default text "loading"
        if (progressDialog != null) {
            progressDialog.show();
            progressDialog.setContentView(R.layout.animated_progress);
        }
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public boolean isProgressShowing() {
        return progressDialog != null && progressDialog.isShowing();
    }

}
