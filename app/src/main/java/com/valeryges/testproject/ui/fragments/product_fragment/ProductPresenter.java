package com.valeryges.testproject.ui.fragments.product_fragment;


import android.app.Application;

import com.valeryges.testproject.TestProjectApplication;
import com.valeryges.testproject.database.repositories.IProductRepository;
import com.valeryges.testproject.events.ReloadSavedEvent;
import com.valeryges.testproject.models.IProduct;
import com.valeryges.testproject.ui.fragments.CommonPresenter;
import com.valeryges.testproject.utils.Logger;
import com.valeryges.testproject.utils.RxBus;
import com.valeryges.testproject.utils.Transformers;

import javax.inject.Inject;

import rx.functions.Action1;

public class ProductPresenter extends CommonPresenter<IProductView> implements IProductPresenter {

    @Inject
    Application application;

    @Inject
    IProductRepository productRepository;

    private boolean isFirstStart = true;

    private final Action1<IProduct> onSuccess = product
            -> {
        showHideProgress(false);
        checkAndCallOrAddToDelayed(productView -> productView.onCheckResult(product));
    };

    private final Action1<Throwable> onError = throwable -> {
        showHideProgress(false);
        Logger.printStackTrace(throwable);
        checkAndCallOrAddToDelayed(productView -> productView.showError(throwable.getMessage()));
    };

    ProductPresenter() {
        super();
        TestProjectApplication.getAppComponent().inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isFirstStart) {
            IProduct product = checkAndCallWithResult(IProductView::getProduct);
            checkProductInDb(product);
            isFirstStart = false;
        }
    }

    private void checkProductInDb(IProduct product) {
        getCompositeSubscription().add(
                productRepository
                        .getByServerId(product.getServerId())
                        .map(productFromDb -> {
                            if (productFromDb != null) {
                                product.setSaved(productFromDb.isSaved());
                            }
                            return product;
                        })
                        .flatMap(productRepository::save)
                        .compose(Transformers.loadAsync())
                        .subscribe(onSuccess, onError));
    }

    @Override
    public void storeProduct(IProduct product) {
        updateProduct(product, true);
    }

    @Override
    public void deleteProduct(IProduct product) {
        updateProduct(product, false);
    }

    private void updateProduct(IProduct product, boolean needToSave) {
        showHideProgress(true);
        product.setSaved(needToSave);
        getCompositeSubscription().add(productRepository
                .save(product)
                .doOnNext(ignored -> RxBus.instance().send(new ReloadSavedEvent()))
                .compose(Transformers.loadAsync())
                .subscribe(onSuccess, onError));
    }
}
