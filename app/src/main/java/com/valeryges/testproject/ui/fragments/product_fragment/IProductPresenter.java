package com.valeryges.testproject.ui.fragments.product_fragment;


import com.valeryges.testproject.models.IProduct;
import com.valeryges.testproject.mvp.IPresenter;

interface IProductPresenter extends IPresenter<IProductView> {
    void storeProduct(IProduct product);
    void deleteProduct(IProduct product);
}
