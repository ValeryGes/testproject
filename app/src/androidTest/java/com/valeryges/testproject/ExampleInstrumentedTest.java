package com.valeryges.testproject;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.valeryges.testproject.database.repositories.CategoryRepository;
import com.valeryges.testproject.database.repositories.ICategoryRepository;
import com.valeryges.testproject.models.Category;
import com.valeryges.testproject.models.ICategory;
import com.valeryges.testproject.utils.Transformers;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.CountDownLatch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    public static Long testId = 666L;
    public static String testName = "testName";
    public static String testLongName = "testLongName";

    private ICategory mCategoryTest;

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.valeryges.testproject", appContext.getPackageName());
    }

    @Before
    @After
    public void prepareDatabase() {
        mCategoryTest = new Category(testId, testLongName);
        ((Category) mCategoryTest).setName(testName);
        ICategoryRepository categoryRepository = new CategoryRepository();
        final CountDownLatch signal = new CountDownLatch(1);
        categoryRepository.remove(mCategoryTest).subscribe(aVoid -> signal.countDown(), throwable -> signal.countDown());
        try {
            signal.await();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCategoryRepository() {
        ICategoryRepository categoryRepository = new CategoryRepository();
        final CountDownLatch signal = new CountDownLatch(1);
        try {
            categoryRepository
                    .save(mCategoryTest)
                    .compose(Transformers.loadAsync())
                    .subscribe(iCategory -> {
                                assertNotNull(iCategory);
                                assertEquals(iCategory.getServerId(), testId);
                                assertEquals(iCategory.getLongName(), testLongName);
                                assertEquals(iCategory.getName(), testName);
                                signal.countDown();
                            },
                            throwable -> {
                                signal.countDown();
                                throw new AssertionError(throwable);
                            });
            signal.await();
            final CountDownLatch signal2 = new CountDownLatch(1);
            categoryRepository
                    .getByServerId(testId)
                    .compose(Transformers.loadAsync())
                    .subscribe(iCategory -> {
                                assertNotNull(iCategory);
                                assertEquals(iCategory.getServerId(), testId);
                                assertEquals(iCategory.getLongName(), testLongName);
                                assertEquals(iCategory.getName(), testName);
                                signal2.countDown();
                            },
                            throwable -> {
                                signal2.countDown();
                                throw new AssertionError(throwable);
                            });
            signal2.await();

        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new AssertionError(e);
        }
    }

}
