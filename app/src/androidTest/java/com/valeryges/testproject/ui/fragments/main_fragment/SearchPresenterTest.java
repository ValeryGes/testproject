package com.valeryges.testproject.ui.fragments.main_fragment;

import android.support.test.runner.AndroidJUnit4;

import com.valeryges.testproject.database.repositories.CategoryRepository;
import com.valeryges.testproject.database.repositories.ICategoryRepository;
import com.valeryges.testproject.models.Category;
import com.valeryges.testproject.models.ICategory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(AndroidJUnit4.class)
public class SearchPresenterTest {

    public static Long testId = 666L;
    public static String testName = "testName";
    public static String testLongName = "testLongName";

    private ICategory mCategoryTest;

    @Before
    @After
    public void prepareDatabase() {
        mCategoryTest = new Category(testId, testLongName);
        ((Category) mCategoryTest).setName(testName);
        ICategoryRepository categoryRepository = new CategoryRepository();
        final CountDownLatch signal = new CountDownLatch(1);
        categoryRepository.remove(mCategoryTest).subscribe(aVoid -> signal.countDown(), throwable -> signal.countDown());
        try {
            signal.await(10L, TimeUnit.SECONDS);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSearchPresenter() {
        final CountDownLatch signal = new CountDownLatch(1);
        TestSearchView testSearchView = new TestSearchView(object -> {
            assertNotNull(object);
            ICategory iCategory = object.get(0);
            assertNotNull(iCategory);
            assertEquals(testId, iCategory.getServerId());
            assertEquals(testLongName, iCategory.getLongName());
            assertEquals(testName, iCategory.getName());
            signal.countDown();
        }, object -> {
            signal.countDown();
            throw new AssertionError(object);
        });
        testSearchView.runResume();
        try {
            signal.await(10L, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
