package com.valeryges.testproject.ui.fragments.main_fragment;

import android.support.annotation.Nullable;

import com.valeryges.testproject.funcs.Func1;
import com.valeryges.testproject.models.ICategory;

import java.util.List;

public class TestSearchView implements ISearchView {

    public void runResume() {
        SearchPresenter searchPresenter = new SearchPresenter();
        searchPresenter.onPresenterCreated();
        attachPresenter(searchPresenter);
        searchPresenter.attachView(this);
        searchPresenter.onResume();
    }

    private ISearchPresenter mISearchPresenter;

    private Func1<List<ICategory>> mFunc;
    private Func1<String> mFuncError;

    public TestSearchView(Func1<List<ICategory>> func, Func1<String> funcError) {
        mFunc = func;
        mFuncError = funcError;
    }

    @Override
    public void initCategories(List<ICategory> categories) {
        mFunc.call(categories);
    }

    @Override
    public void showAlert(String message) {
        mFuncError.call(message);
    }

    @Override
    public void showProgress() {
        // stub
    }

    @Override
    public void hideProgress() {
        // stub
    }

    @Override
    public void attachPresenter(@Nullable ISearchPresenter presenter) {
        mISearchPresenter = presenter;
    }

    @Nullable
    @Override
    public ISearchPresenter getPresenter() {
        return mISearchPresenter;
    }
}
