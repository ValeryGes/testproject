package com.valeryges.testproject.injections.modules;

import com.valeryges.testproject.ExampleInstrumentedTest;
import com.valeryges.testproject.network.beans.CategoryBean;
import com.valeryges.testproject.network.modules.CategoriesModule;
import com.valeryges.testproject.network.modules.ICategoriesModule;
import com.valeryges.testproject.network.responces.CategoryResponse;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import rx.Observable;

@Module
public class TestNetworkModule extends NetworkModule {

    @Singleton
    @Provides
    ICategoriesModule provideCategoriesModule(Retrofit retrofit) {
        return new CategoriesModule(apikey -> Observable.just(new CategoryResponse())
                .doOnNext(categoryResponse -> {
                    List<CategoryBean> categoryBeans = new ArrayList<>();
                    CategoryBean categoryBean = new CategoryBean();
                    categoryBean.setCategoryId(ExampleInstrumentedTest.testId);
                    categoryBean.setName(ExampleInstrumentedTest.testName);
                    categoryBean.setLongName(ExampleInstrumentedTest.testLongName);
                    categoryBeans.add(categoryBean);
                    categoryResponse.setResults(categoryBeans);
                }));
    }


}
