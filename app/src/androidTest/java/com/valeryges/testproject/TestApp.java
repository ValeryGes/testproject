package com.valeryges.testproject;

import com.valeryges.testproject.injections.modules.TestNetworkModule;
import com.valeryges.testproject.injections.components.AppComponent;
import com.valeryges.testproject.injections.components.DaggerAppComponent;
import com.valeryges.testproject.injections.modules.AppModule;
import com.valeryges.testproject.injections.modules.DataBaseModule;

/**
 * Created by vges on 6/18/2018.
 */
public class TestApp extends TestProjectApplication {

    @Override
    protected AppComponent initDagger() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new TestNetworkModule())
                .dataBaseModule(new DataBaseModule())
                .build();
    }
}
